namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductHistory")]
    public partial class ProductHistory
    {
        public long Id { get; set; }

        [StringLength(50)]
        public string ASIN { get; set; }

        [StringLength(50)]
        public string ColumnName { get; set; }

        public string ColumnValue { get; set; }

        [StringLength(180)]
        public string AmazonSellerId { get; set; }

        public DateTime? DeletedOn { get; set; }

        public DateTime? AddedOn { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }

        public virtual Seller Seller { get; set; }
    }
}
