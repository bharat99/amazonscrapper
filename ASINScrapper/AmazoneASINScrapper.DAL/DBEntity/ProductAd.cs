namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProductAd
    {
        public long Id { get; set; }

        [StringLength(50)]
        public string ASIN { get; set; }

        [StringLength(250)]
        public string AdsURL { get; set; }

        [StringLength(180)]
        public string SellerUserId { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }

        public virtual Seller Seller { get; set; }
    }
}
