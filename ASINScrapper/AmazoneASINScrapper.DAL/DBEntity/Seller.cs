namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Seller")]
    public partial class Seller
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Seller()
        {
            ProductAds = new HashSet<ProductAd>();
            ProductDetails = new HashSet<ProductDetail>();
            ProductHistories = new HashSet<ProductHistory>();
            ProductSellerMappings = new HashSet<ProductSellerMapping>();
            SellerHistories = new HashSet<SellerHistory>();
        }

        [Key]
        [StringLength(180)]
        public string AmazonSellerId { get; set; }

        [StringLength(250)]
        public string ProfileImageURL { get; set; }

        [StringLength(100)]
        public string SellerName { get; set; }

        [StringLength(200)]
        public string SellerContactDetails { get; set; }

        public string Description { get; set; }

        [StringLength(250)]
        public string SellerFrontURL { get; set; }

        [StringLength(250)]
        public string SellerShopURL { get; set; }

        [StringLength(50)]
        public string Rating { get; set; }

        public long? Positive_30days { get; set; }

        public long? Positive_90days { get; set; }

        public long? Positive_12Month { get; set; }

        public long? Positive_LifeTime { get; set; }

        public long? Negative_30days { get; set; }

        public long? Negative_90days { get; set; }

        public long? Negative_12Month { get; set; }

        public long? Negative_LifeTime { get; set; }

        public long? Neutral_30days { get; set; }

        public long? Neutral_90days { get; set; }

        public long? Neutral_12Month { get; set; }

        public long? Neutral_LifeTime { get; set; }

        public string WhyChooseUs { get; set; }

        [StringLength(50)]
        public string CustomerServicePhone { get; set; }

        [StringLength(50)]
        public string CustomerServiceHours { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public DateTime? DeletedOn { get; set; }
        public DateTime? AddedOn { get; set; }

        public int? SellerProductCount { get; set; }
        public string MarketPlaceID { get; set; }

        public string Review { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductAd> ProductAds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductDetail> ProductDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductHistory> ProductHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductSellerMapping> ProductSellerMappings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SellerHistory> SellerHistories { get; set; }
    }
}
