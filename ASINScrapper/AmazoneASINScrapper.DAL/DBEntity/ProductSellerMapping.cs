namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductSellerMapping")]
    public partial class ProductSellerMapping
    {
        public long ID { get; set; }

        [StringLength(50)]
        public string ASIN { get; set; }

        [StringLength(50)]
        public string Price { get; set; }

        [StringLength(50)]
        public string Condition { get; set; }

        [StringLength(180)]
        public string AmazonSellerId { get; set; }

        public bool? IsPrime { get; set; }

        public bool? IsFulfillmentbyAmazon { get; set; }

        public bool? IsAtoZGaurantee { get; set; }

        [StringLength(400)]
        public string DeliveryDetail { get; set; }
        public DateTime? AddedOn { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }

        public virtual Seller Seller { get; set; }
    }
}
