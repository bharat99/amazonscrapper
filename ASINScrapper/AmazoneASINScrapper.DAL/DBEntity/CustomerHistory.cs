namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CustomerHistory")]
    public partial class CustomerHistory
    {
        public long Id { get; set; }

        [StringLength(180)]
        public string AmazonUserId { get; set; }

        [StringLength(50)]
        public string ColumnName { get; set; }

        public string ColumnValue { get; set; }

        public DateTime? AddedOn { get; set; }

        public DateTime? DeletedOn { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
