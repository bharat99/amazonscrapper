namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AmazonCategory")]
    public partial class AmazonCategory
    {
        [StringLength(50)]
        public string AmazonCategoryId { get; set; }

        [StringLength(50)]
        public string CategoryName { get; set; }

        [StringLength(100)]
        public string CategoryLink { get; set; }

        [StringLength(30)]
        public string ParentId { get; set; }

        public DateTime? createdOn { get; set; }

        public DateTime? updatedOn { get; set; }
    }
}
