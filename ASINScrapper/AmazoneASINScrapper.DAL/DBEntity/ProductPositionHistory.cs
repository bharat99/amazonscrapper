namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductPositionHistory")]
    public partial class ProductPositionHistory
    {
        public long Id { get; set; }

        [StringLength(50)]
        public string ASIN { get; set; }

        public int? ProductPosition { get; set; }

        public DateTime? Addedon { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }
    }
}
