namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SellerProduct
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SellerProduct()
        {
            ByFeatures = new HashSet<ByFeature>();
            ByFeatureHistories = new HashSet<ByFeatureHistory>();
            CustomerGroupInterestsHistories = new HashSet<CustomerGroupInterestsHistory>();
            CustomerGroupsInterests = new HashSet<CustomerGroupsInterest>();
            CustomerReviews = new HashSet<CustomerReview>();
            ProductAds = new HashSet<ProductAd>();
            ProductBSRs = new HashSet<ProductBSR>();
            ProductBSRHistories = new HashSet<ProductBSRHistory>();
            ProductSellerMappings = new HashSet<ProductSellerMapping>();
            ProductHistories = new HashSet<ProductHistory>();
            ProductKeyFeatures = new HashSet<ProductKeyFeature>();
            ProductMetaKeyWords = new HashSet<ProductMetaKeyWord>();
            ProductPositionHistories = new HashSet<ProductPositionHistory>();
            ProductQuestions = new HashSet<ProductQuestion>();
            TechnicalDetails = new HashSet<TechnicalDetail>();
        }

        [Key]
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string ASIN { get; set; }
        [Required]
        [StringLength(180)]
        public string AmazonSellerId { get; set; }
        [StringLength(100)]
        public string Qid { get; set; }

        [StringLength(250)]
        public string Title { get; set; }

        public string Description { get; set; }

        [StringLength(250)]
        public string ImageLink { get; set; }

        [StringLength(250)]
        public string ImageLink1 { get; set; }

        [StringLength(250)]
        public string ImageLink2 { get; set; }

        [StringLength(250)]
        public string ImageLink3 { get; set; }

        [StringLength(250)]
        public string ImageLink4 { get; set; }

        [StringLength(250)]
        public string ImageLink5 { get; set; }

        [StringLength(250)]
        public string ImageLink6 { get; set; }

        [StringLength(250)]
        public string VideoLink { get; set; }

        [StringLength(50)]
        public string Price { get; set; }

        [StringLength(50)]
        public string ListPrice { get; set; }

        [StringLength(50)]
        public string YouSave { get; set; }

        [StringLength(250)]
        public string ProductCategory { get; set; }

        public int? ProductPosition { get; set; }

        public decimal? CustomerRating { get; set; }

        public int? CustomerRatingCount { get; set; }

        public bool? IsPrime { get; set; }

        
        

        public bool? IsAmazonChoice { get; set; }

        [StringLength(400)]
        public string WarrentyAndSupport { get; set; }

        public decimal? ShippingWeight { get; set; }

        public string ShippingDetails { get; set; }

        public bool? Is10daysReplacement { get; set; }

        public bool? Is30daysReplacement { get; set; }

        public bool? Is60daysReplacement { get; set; }

        [StringLength(50)]
        public string ProductDimensions { get; set; }

        [StringLength(50)]
        public string ItemModelNumber { get; set; }

        [StringLength(50)]
        public string ItemWeight { get; set; }

        [StringLength(50)]
        public string DateFirstListedOnAmazon { get; set; }

        public DateTime? DeletedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }
        public DateTime? AddedOn { get; set; }

        public bool? IsInStock { get; set; }

        public bool? IsAplusContent { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ByFeature> ByFeatures { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ByFeatureHistory> ByFeatureHistories { get; set; }

        public virtual ComboBoxDealHistory ComboBoxDealHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerGroupInterestsHistory> CustomerGroupInterestsHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerGroupsInterest> CustomerGroupsInterests { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerReview> CustomerReviews { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductAd> ProductAds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductBSR> ProductBSRs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductBSRHistory> ProductBSRHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductSellerMapping> ProductSellerMappings { get; set; }

        public virtual Seller Seller { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductHistory> ProductHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductKeyFeature> ProductKeyFeatures { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductMetaKeyWord> ProductMetaKeyWords { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductPositionHistory> ProductPositionHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductQuestion> ProductQuestions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TechnicalDetail> TechnicalDetails { get; set; }
    }
}
