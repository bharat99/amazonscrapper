namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CustomerReview")]
    public partial class CustomerReview
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CustomerReview()
        {
            ReviewCommentHistories = new HashSet<ReviewCommentHistory>();
        }

        [Key]
        [StringLength(180)]
        public string AmazonReviewId { get; set; }

        [StringLength(50)]
        public string ASIN { get; set; }

        [StringLength(350)]
        public string ReviewTitle { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ReviewDate { get; set; }

        public decimal? ReviewRating { get; set; }

        public string Description { get; set; }

        [StringLength(150)]
        public string Color { get; set; }

        [StringLength(150)]
        public string Size { get; set; }

        public int? PeopleHelpfulCount { get; set; }

        public bool? IsVerifiedPurchase { get; set; }

        public bool? IsPositiveReview { get; set; }

        public bool? IsEarlyReviewerRewards { get; set; }

        [StringLength(180)]
        public string ReviewedBy { get; set; }

        public DateTime? DeletedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }
        public DateTime? AddedOn { get; set; }

        public virtual Customer Customer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReviewCommentHistory> ReviewCommentHistories { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }
    }
}
