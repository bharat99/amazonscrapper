namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SearchKeyWordResult")]
    public partial class SearchKeyWordResult
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long KeyWordId { get; set; }

        [StringLength(50)]
        public string KeyWord { get; set; }

        public long? ParentId { get; set; }

        public long? MasterId { get; set; }

        public DateTime? createdOn { get; set; }

        public DateTime? updatedOn { get; set; }
    }
}
