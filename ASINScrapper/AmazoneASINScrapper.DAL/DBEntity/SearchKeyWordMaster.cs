namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SearchKeyWordMaster")]
    public partial class SearchKeyWordMaster
    {
        [Key]
        public long MaterId { get; set; }

        [StringLength(50)]
        public string KeyWord { get; set; }

        public DateTime? createdOn { get; set; }

        public DateTime? updatedOn { get; set; }
    }
}
