namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ReviewCommentHistory")]
    public partial class ReviewCommentHistory
    {
        public long Id { get; set; }

        [StringLength(180)]
        public string AmazonReviewId { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(180)]
        public string CommentedBy { get; set; }

        [Column(TypeName = "date")]
        public DateTime? CommentedOn { get; set; }

        public DateTime? DeletedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual CustomerReview CustomerReview { get; set; }
    }
}
