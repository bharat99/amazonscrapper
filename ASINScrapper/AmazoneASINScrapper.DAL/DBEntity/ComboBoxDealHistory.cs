namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ComboBoxDealHistory")]
    public partial class ComboBoxDealHistory
    {
        [Key]
        [StringLength(50)]
        public string ASIN { get; set; }

        [StringLength(50)]
        public string CobmoboxASIN { get; set; }

        public string Discount { get; set; }

        [StringLength(50)]
        public string Price { get; set; }

        public DateTime? AddedOn { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }
    }
}
