namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ByFeatureHistory")]
    public partial class ByFeatureHistory
    {
        public long Id { get; set; }

        [StringLength(50)]
        public string ASIN { get; set; }

        [StringLength(150)]
        public string Attribute { get; set; }

        public decimal? Value { get; set; }

        public DateTime? AddedOn { get; set; }

        public DateTime? DeletedOn { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }
    }
}
