namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TechnicalDetail
    {
        public long Id { get; set; }

        [StringLength(50)]
        public string ASIN { get; set; }

        [StringLength(150)]
        public string Attribute { get; set; }

        [StringLength(150)]
        public string Value { get; set; }

        public DateTime? DeletedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public DateTime? AddedOn { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }
    }
}
