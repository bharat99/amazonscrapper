namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductBSR")]
    public partial class ProductBSR
    {
        [Key]
        public long BSRId { get; set; }

        [StringLength(50)]
        public string ASIN { get; set; }

        public long? BestSellerRank { get; set; }

        [StringLength(100)]
        public string BestSellerInCategory { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }
    }
}
