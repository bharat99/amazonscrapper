namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductKeyFeature")]
    public partial class ProductKeyFeature
    {
        [Key]
        public long KeyFeatureId { get; set; }

        [StringLength(50)]
        public string ASIN { get; set; }

        [StringLength(1000)]
        public string KeyFetaure { get; set; }

        public bool? IsHidden { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }
    }
}
