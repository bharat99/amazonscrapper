namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductQuestion")]
    public partial class ProductQuestion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProductQuestion()
        {
            QAMappings = new HashSet<QAMapping>();
        }

        [Key]
        [StringLength(50)]
        public string QuestionId { get; set; }

        [StringLength(50)]
        public string ASIN { get; set; }

        [StringLength(250)]
        public string Question { get; set; }

        public int? NumberOfVote { get; set; }

        public long? UpdatedOn { get; set; }

        public long? DeletedOn { get; set; }

        public DateTime? AddedOn { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QAMapping> QAMappings { get; set; }
    }
}
