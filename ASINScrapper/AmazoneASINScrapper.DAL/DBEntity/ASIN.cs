namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ASIN
    {
        public long ID { get; set; }

        [Column("ASIN")]
        [StringLength(50)]
        public string ASIN1 { get; set; }

        public bool? IsScheduled { get; set; }

        public DateTime? ScheduleOn { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public DateTime? StartedOn { get; set; }
        public DateTime? EndOn { get; set; }

        public bool? customer { get; set; }

        public bool? seller { get; set; }

        public bool? product { get; set; }

        public bool? CustomerReview { get; set; }

        public bool? questionAns { get; set; }

        public bool? ProductSellerMapping { get; set; }

        public bool? bestSellerRank { get; set; }

        public bool? KeyFearture { get; set; }

        public bool? ByFeature { get; set; }

        public bool? CustomerGroupsInterest { get; set; }

        public bool? TechnicalDetail { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }
    }
}
