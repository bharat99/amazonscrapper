namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Customer")]
    public partial class Customer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Customer()
        {
            ReviewCommentHistories = new HashSet<ReviewCommentHistory>();
            CustomerHistories = new HashSet<CustomerHistory>();
            CustomerReviews = new HashSet<CustomerReview>();
        }

        [Key]
        [StringLength(180)]
        public string AmazonUserId { get; set; }

        [StringLength(350)]
        public string CustomerFrontURL { get; set; }

        [StringLength(350)]
        public string ProfileImageURL { get; set; }

        [StringLength(150)]
        public string Name { get; set; }

        [StringLength(250)]
        public string Designation { get; set; }

        public string About { get; set; }

        public long? ReviewRanking { get; set; }

        public int? HelpfulVotes { get; set; }

        public int? Reviews { get; set; }

        public int? Hearts { get; set; }

        public int? IdeaLists { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public DateTime? DeletedOn { get; set; }

        public DateTime? AddedOn { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReviewCommentHistory> ReviewCommentHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerHistory> CustomerHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerReview> CustomerReviews { get; set; }
    }
}
