namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProductMetaKeyWord
    {
        [Key]
        public long MetaKeyId { get; set; }

        [StringLength(50)]
        public string ASIN { get; set; }

        [StringLength(200)]
        public string MetaKey { get; set; }

        [StringLength(200)]
        public string MetaTile { get; set; }

        [StringLength(350)]
        public string MetaDescription { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }
    }
}
