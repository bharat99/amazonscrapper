namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductBSRHistory")]
    public partial class ProductBSRHistory
    {
        [Key]
        public long BSRHistoryId { get; set; }

        [StringLength(50)]
        public string ASIN { get; set; }

        [StringLength(50)]
        public string ColumnName { get; set; }

        [StringLength(100)]
        public string ColumnValue { get; set; }

        public DateTime? AddedOn { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }
    }
}
