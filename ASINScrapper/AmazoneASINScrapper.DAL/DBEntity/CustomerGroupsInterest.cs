namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CustomerGroupsInterest
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CustomerGroupsInterest()
        {
            CustomerGroupInterestsHistories = new HashSet<CustomerGroupInterestsHistory>();
        }

        public long Id { get; set; }

        [StringLength(50)]
        public string ASIN { get; set; }

        [StringLength(150)]
        public string Attribute { get; set; }

        public decimal? Value { get; set; }

        public DateTime? DeletedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public DateTime? AddedOn { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerGroupInterestsHistory> CustomerGroupInterestsHistories { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }
    }
}
