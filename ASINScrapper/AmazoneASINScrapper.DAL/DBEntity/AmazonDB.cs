namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AmazonDB : DbContext
    {
        public AmazonDB()
            : base("name=AmazonDB")
        {
        }

        public virtual DbSet<AmazonCategory> AmazonCategories { get; set; }
        public virtual DbSet<ASIN> ASINs { get; set; }
        public virtual DbSet<ByFeature> ByFeatures { get; set; }
        public virtual DbSet<ByFeatureHistory> ByFeatureHistories { get; set; }
        public virtual DbSet<ComboBoxDealHistory> ComboBoxDealHistories { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerGroupInterestsHistory> CustomerGroupInterestsHistories { get; set; }
        public virtual DbSet<CustomerGroupsInterest> CustomerGroupsInterests { get; set; }
        public virtual DbSet<CustomerHistory> CustomerHistories { get; set; }
        public virtual DbSet<CustomerReview> CustomerReviews { get; set; }
        public virtual DbSet<ProductAd> ProductAds { get; set; }
        public virtual DbSet<ProductBSR> ProductBSRs { get; set; }
        public virtual DbSet<ProductBSRHistory> ProductBSRHistories { get; set; }
        public virtual DbSet<ProductDetail> ProductDetails { get; set; }
        public virtual DbSet<ProductHistory> ProductHistories { get; set; }
        public virtual DbSet<ProductKeyFeature> ProductKeyFeatures { get; set; }
        public virtual DbSet<ProductMetaKeyWord> ProductMetaKeyWords { get; set; }
        public virtual DbSet<ProductPositionHistory> ProductPositionHistories { get; set; }
        public virtual DbSet<ProductQuestion> ProductQuestions { get; set; }
        public virtual DbSet<ProductSellerMapping> ProductSellerMappings { get; set; }
        public virtual DbSet<QAMapping> QAMappings { get; set; }
        public virtual DbSet<ReviewCommentHistory> ReviewCommentHistories { get; set; }
        public virtual DbSet<SearchKeyWordMaster> SearchKeyWordMasters { get; set; }
        public virtual DbSet<SearchKeyWordResult> SearchKeyWordResults { get; set; }
        public virtual DbSet<Seller> Sellers { get; set; }
        public virtual DbSet<SellerHistory> SellerHistories { get; set; }
        public virtual DbSet<TechnicalDetail> TechnicalDetails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ASIN>()
                .Property(e => e.ASIN1)
                .IsUnicode(false);

            modelBuilder.Entity<ASIN>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.ReviewCommentHistories)
                .WithOptional(e => e.Customer)
                .HasForeignKey(e => e.CommentedBy);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.CustomerReviews)
                .WithOptional(e => e.Customer)
                .HasForeignKey(e => e.ReviewedBy);

            modelBuilder.Entity<CustomerGroupsInterest>()
                .HasMany(e => e.CustomerGroupInterestsHistories)
                .WithOptional(e => e.CustomerGroupsInterest)
                .HasForeignKey(e => e.CustomerGroupsInterestsId);

            modelBuilder.Entity<CustomerReview>()
                .Property(e => e.ReviewTitle)
                .IsFixedLength();

            modelBuilder.Entity<CustomerReview>()
                .Property(e => e.Color)
                .IsFixedLength();

            modelBuilder.Entity<CustomerReview>()
                .Property(e => e.Size)
                .IsFixedLength();

            modelBuilder.Entity<ProductAd>()
                .Property(e => e.AdsURL)
                .IsFixedLength();

            modelBuilder.Entity<ProductDetail>()
                .HasOptional(e => e.ComboBoxDealHistory)
                .WithRequired(e => e.ProductDetail);

            modelBuilder.Entity<Seller>()
                .HasMany(e => e.ProductAds)
                .WithOptional(e => e.Seller)
                .HasForeignKey(e => e.SellerUserId);

            modelBuilder.Entity<Seller>()
                .HasMany(e => e.ProductDetails)
                .WithRequired(e => e.Seller)
                .WillCascadeOnDelete(false);
        }
    }
}
