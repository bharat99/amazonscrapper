namespace AmazoneASINScrapper.DAL.DBEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("QAMapping")]
    public partial class QAMapping
    {
        public long Id { get; set; }

        public string Answer { get; set; }

        [StringLength(50)]
        public string QuestionId { get; set; }

        [StringLength(180)]
        public string AnsweredBy { get; set; }

        [Column(TypeName = "date")]
        public DateTime? AnsweredOn { get; set; }

        public DateTime? DeletedOn { get; set; }

        public DateTime? AddedOn { get; set; }

        public virtual ProductQuestion ProductQuestion { get; set; }
    }
}
