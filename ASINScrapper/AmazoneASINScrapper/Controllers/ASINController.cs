﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AmazoneASINScrapper.BAL.Scrapper;
using AmazoneASINScrapper.DAL.DBEntity;
using System.Threading.Tasks;
using ScrapingHelp;
using Amib.Threading;
using AmazoneASINScrapper.BAL.BusinessRepo;
using System.Configuration;
using AmazoneASINScrapper.BAL.helper;

namespace AmazoneASINScrapper.Controllers
{

    public class ASINController : ApiController
    {
        public void getASINsDetail(string site, string ASIN, string isreview, string isqna, string isseller)
       {


            string domainUrl = "https://www.amazon.de/";

            var _siteVal = ConfigurationManager.AppSettings[site];

            if (_siteVal != null)
            {
                domainUrl = ConfigurationManager.AppSettings[site].ToString();
            }

            string[] ASINs = ASIN.Split(',');
            //for (int i = 0; i < ASINs.Length; i++)
            //{
            //    scrapASINInfo(domainUrl, ASINs[i], isreview, isqna, isseller);
            //}
            Parallel.ForEach(ASINs, (_asin) =>
            { scrapASINInfo(domainUrl, _asin, isreview, isqna, isseller); });

        }



        [NonAction]
        public ProductDetail scrapASINInfo(string domainUrl, string ASIN, string isreview, string isqna, string isseller)
        {
            ProductDetail p = new ProductDetail();
            sellerInfo clsseller = new sellerInfo();
            productReviewInfo clsreview = new productReviewInfo();
            QnAInfo clsqna = new QnAInfo();
            List<Seller> seller = new List<Seller>();
            List<Customer> customer = new List<Customer>();
            ProductRepo repo = new ProductRepo();

            if (ASIN != null)
            {
                timeLogger.updateCreateScheduleTable(ASIN);

                if (domainUrl.Contains(".de"))
                {
                    productInfoDe cls = new productInfoDe();
                    ProductDetail p2 = new ProductDetail();
                    p2 = cls.getProductDetail(domainUrl, ASIN);
                    return p2;
                }
                if (domainUrl.Contains(".in"))
                {
                    ProductInfoIn cls = new ProductInfoIn();
                    //ProductDetail p3 = new ProductDetail();
                    p = cls.getProductDetail(domainUrl, ASIN);
                }
                if (domainUrl.Contains(".com"))
                {
                    productInfo clsproduct = new productInfo();
                    p = clsproduct.getProductDetail(domainUrl, ASIN);
                }

                if (p.ASIN != null)
                {
                    if (isseller == "1")
                    {
                        p.ProductSellerMappings = clsseller.getproductSellerMapping(domainUrl, ASIN, out seller,p.AmazonSellerId);
                    }
                    if (isreview == "1")
                    {
                        List<CustomerReview> lstReviews= repo.getCustomerReview(ASIN);
                        p.CustomerReviews = clsreview.getCutomerReview(domainUrl, ASIN, out customer, lstReviews);
                    }
                    if (isqna == "1")
                    {
                        p.ProductQuestions = clsqna.getFAQ(domainUrl, ASIN);
                    }
                }
                
                //Task<CustomerReview> someAsyncOp1 = clsreview.getCutomerReview(ASIN, out customer);
                //Task<SomeType2> someAsyncOp2 = SomeAsyncOperation2();
                //Task<SomeType3> someAsyncOp3 = SomeAsyncOperation3();
                //Task<SomeType4> someAsyncOp4 = SomeAsyncOperation4();

               
                repo.saveScrapedDetail(p, customer, seller);
                #region Save seller products
                //AmazoneASINScrapper.BAL.Scrapper.SellerProduct sp = new AmazoneASINScrapper.BAL.Scrapper.SellerProduct();
                //foreach (var sel in seller)
                //{
                //    if (!string.IsNullOrEmpty(sel.MarketPlaceID))
                //    {
                //        sp.saveSellerProducts(domainUrl, sel.AmazonSellerId, sel.MarketPlaceID, sel.SellerProductCount);
                //    }
                //}
                #endregion
            }
            return p;
        }
    }
}
