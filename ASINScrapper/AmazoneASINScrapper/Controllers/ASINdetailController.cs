﻿using AmazoneASINScrapper.DAL.DBEntity;
using AmazoneASINScrapper.Models;
using Amib.Threading;
using HtmlAgilityPack;
using ScrapingHelp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace AmazoneASINScrapper.Controllers
{
    public class ASINdetailController : ApiController
    {
        int ProxyChange = 0;
        ProductDetail p;

        public List<ProductDetail> getASINsDetail(string ASIN, string isreview, string isqna, string isseller)
        {
            List<ProductDetail> lstProductDetail = new List<ProductDetail>();
            string[] ASINs = ASIN.Split(',');
            //SmartThreadPool obj = new SmartThreadPool(ASINs.Length);
            //for (int i = 0; i < ASINs.Length; i++)
            //{
            //    string AsinNumber = ASINs[i].Trim();
            //    obj.QueueWorkItem(getProductDetail(AsinNumber, isreview, isqna, isseller));
            //}
            for (int i = 0; i < ASINs.Length; i++)
            {
                DateTime start = DateTime.Now;
                getProductDetail(ASINs[i], isreview, isqna, isseller);
                try
                {
                    DateTime end = DateTime.Now;
                    // Your code...
                    // Could also be before try if you know the exception occurs in SaveChanges

                    using (DBModels context = new DBModels())
                    {
                        var a = p.CustomerReviews;
                        context.ProductDetails.Add(p);
                        context.SaveChanges();
                    }
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }

            }
            //Parallel.ForEach(ASINs, (product) => { lstProductDetail.Add(getProductDetail(ASIN, isreview, isqna, isseller)); });

            return lstProductDetail;
        }

        #region BASIC PRODUCT INFORMATION
        [NonAction]
        public ProductDetail getProductDetail(string ASIN, string isreview, string isqna, string isseller)
        {

            string mainUrl = "https://www.amazon.com/dp/" + ASIN;

            int retry = Convert.ToInt32(ConfigurationManager.AppSettings["_retry"]);
            retry_mainsource:
            var mainsource = GetSource(mainUrl, "");
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(mainsource);

            #region RETRY if Source is Empty
            if (mainsource == "" && retry < 5)
            {
                retry++;

                goto retry_mainsource;
            }
            else if (mainsource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
            {
                retry++;

                goto retry_mainsource;
            }
            #endregion


            #region check For SellerID

            string AmazonSellersId = string.Empty;
            var sellerDiv = doc.GetElementbyId("merchant-info");
            if (sellerDiv == null)
            {
                int retry2 = Convert.ToInt32(ConfigurationManager.AppSettings["_retry"]);
                sellerProfileSource_retry2:
                mainsource = GetSource(mainUrl + "?psc=1", "");
                #region RETRY if Source is Empty
                if (mainsource == "" && retry < 5)
                {
                    retry2++;

                    goto sellerProfileSource_retry2;

                }
                else if (mainsource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
                {
                    retry2++;

                    goto sellerProfileSource_retry2;
                }
                #endregion
                doc = new HtmlDocument();
                doc.LoadHtml(mainsource);

            }

            #endregion

            string urlReview = HtmlHelper.ReturnValue(mainsource, "class=\"a-link-emphasis a-text-bold\"", "href=\"", "\">");
            //string urlFaq = HtmlHelper.ReturnValue(mainsource, "class=\"cdQuestionLazySeeAll\"", "href=\"", "\">");
            //string sellerUrl = HtmlHelper.ReturnValue(mainsource, "class=\"cdQuestionLazySeeAll\"", "href=\"", "\">");

            p = new ProductDetail();
            productDetail(mainsource, doc, ASIN);
            //productTechnicalDetail(mainsource, ASIN, doc);

            //getproductSellerMapping(ASIN);

            //productDetail(mainsource, doc, ASIN);
            //productTechnicalDetail(mainsource, ASIN, doc);
            ////productByFeatureDetail(mainsource, ASIN);
            //getproductSellerMapping(ASIN);
            //getCutomerReview(urlReview, ASIN);
            //getFAQ(ASIN);
            int noOfActions = 1;

            if (isreview == "1")
                noOfActions++;
            if (isqna == "1")
                noOfActions++;
            if (isseller == "1")
                noOfActions++;

            Action[] actions = new Action[noOfActions];
            actions[0] = () => productDetail(mainsource, doc, ASIN);
            actions[1] = () => productTechnicalDetail(mainsource, ASIN, doc);
            for (int i = 2; i < actions.Length; i++)
            {
                if (isreview == "1")
                {
                    actions[i] = () => getCutomerReview(urlReview, ASIN);
                    isreview = "0";
                }
                else if (isqna == "1")
                {
                    actions[i] = () => getFAQ(ASIN);
                    isqna = "0";
                }
                else if (isseller == "1")
                {
                    actions[i] = () => getproductSellerMapping(ASIN);
                    isseller = "0";
                }
            }


            //Parallel.Invoke(actions);

            //Parallel.Invoke(() => { productDetail(mainsource, doc, ASIN); }, () => { productTechnicalDetail(mainsource, ASIN, doc); },
            //    () => { getproductSellerMapping(ASIN); }, () => { getCutomerReview(urlReview, ASIN); }, () => { getFAQ(ASIN); });

            return p;
        }
        [NonAction]
        public void productDetail(string mainsource, HtmlDocument doc, string ASIN)
        {

            p.ASIN = ASIN;

            p.Title = HtmlHelper.ReturnValue(mainsource, "id=\"productTitle\" class=\"a-size-large\">", "</");
            if (p.Title == "")
            {
                p.Title = HtmlHelper.ReturnValue(mainsource, "<div id=\"item_name\">", "</");
            }
            if (p.Title == "")
            {
                p.Title = HtmlHelper.ReturnValue(mainsource, "<span id=\"productTitle\"", ">", "<");
            }
            var Description_elm = doc.GetElementbyId("productDescription");
            if (Description_elm != null)
                p.Description = Regex.Replace(Description_elm.InnerHtml, "<.*?>", string.Empty);

            var elm = doc.GetElementbyId("altImages");

            if (elm != null)
            {
                string short_src = elm.InnerHtml;
                string[] imageArray = short_src.Split(new string[] { "<li" }, StringSplitOptions.None);
                if (imageArray.Length > 2)
                {
                    p.ImageLink = HtmlHelper.ReturnValue(imageArray[1], "<img", "src=\"", "\"");
                    if (imageArray.Length > 3)
                        p.ImageLink1 = HtmlHelper.ReturnValue(imageArray[2], "<img", "src=\"", "\"");
                    if (imageArray.Length > 4)
                        p.ImageLink2 = HtmlHelper.ReturnValue(imageArray[3], "<img", "src=\"", "\"");
                    if (imageArray.Length > 5)
                        p.ImageLink3 = HtmlHelper.ReturnValue(imageArray[4], "<img", "src=\"", "\"");
                    if (imageArray.Length > 6)
                        p.ImageLink4 = HtmlHelper.ReturnValue(imageArray[5], "<img", "src=\"", "\"");
                    if (imageArray.Length > 7)
                        p.ImageLink5 = HtmlHelper.ReturnValue(imageArray[6], "<img", "src=\"", "\"");
                    if (imageArray.Length > 8)
                        p.ImageLink6 = HtmlHelper.ReturnValue(imageArray[7], "<img", "src=\"", "\"");

                    p.VideoLink = HtmlHelper.ReturnValue(short_src, "class=\"a-spacing-small item videoBlockIngress videoBlockDarkIngress\"", "src=\"", "\"");
                }
            }

            p.Price = HtmlHelper.formatString(HtmlHelper.ReturnValue(mainsource, "id=\"priceblock_ourprice\"", "\">", "</span>")).Trim().Replace("$", "");
            p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "<span class=\"a-size-large a-color-price\">", "</").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "id=\"priceblock_ourprice\"", ">", "</").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "id=\"priceblock_dealprice\"", ">", "</").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "id=\"priceblock_dealprice\"", ">", "</").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "class=\"a-size-large a-color-price\"", ">", "<").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "\"sims-fbt-this-item a-text-bold\"", "'p13n-sc-price'>", "<").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "Price:", "class=\"a-size-medium a-color-price\">", "<").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }

            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.formatString(HtmlHelper.ReturnValue(mainsource, "\"verticalAlign a-size-large\">", "</div>")).Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
                if (!p.Price.Contains(".") && p.Price.Length > 2)
                {
                    string s = p.Price.Substring(p.Price.Length - 2);
                    string m = p.Price.Substring(0, p.Price.Length - 2).Replace("  ", "").Replace(" ", "");
                    string t = "." + s;
                    p.Price = m + t;
                }
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.formatString(HtmlHelper.ReturnValue(mainsource, "from&nbsp;", "</div>")).Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                string sortprice = HtmlHelper.ReturnValue(mainsource, "priceInfoData=", "/span>");
                p.Price = HtmlHelper.ReturnValue(sortprice, "\">", "<");
            }

            p.ListPrice = HtmlHelper.ReturnValue(mainsource, "class=\"a-size-small a-text-strike\">", "</span>").Trim().Replace("$", "");
            p.ListPrice = Regex.Replace(p.ListPrice, "<.*?>", string.Empty);
            if (p.ListPrice == string.Empty)
            {
                p.ListPrice = HtmlHelper.ReturnValue(mainsource, "List Price:", "class=\"a-text-strike\">", "<").Trim().Replace("$", "");
                p.ListPrice = Regex.Replace(p.ListPrice, "<.*?>", string.Empty);
            }
            if (p.ListPrice == string.Empty)
            {
                p.ListPrice = HtmlHelper.ReturnValue(mainsource, "List Price:", "class=\"a-text-strike\">", "<").Trim().Replace("$", "");
                p.ListPrice = Regex.Replace(p.ListPrice, "<.*?>", string.Empty);
            }
            if (p.ListPrice == string.Empty)
            {
                p.ListPrice = HtmlHelper.ReturnValue(mainsource, "Was Price:", "class=\"a-text-strike\">", "<").Trim().Replace("$", "");
                p.ListPrice = Regex.Replace(p.ListPrice, "<.*?>", string.Empty);
            }
            if (p.ListPrice == string.Empty)
            {
                p.ListPrice = HtmlHelper.ReturnValue(mainsource, "class=\"a-size-small a-text-strike\"", ">", "<").Trim().Replace("$", "");
                p.ListPrice = Regex.Replace(p.ListPrice, "<.*?>", string.Empty);
            }
            if (p.ListPrice == string.Empty || p.ListPrice == null)
            {
                p.ListPrice = HtmlHelper.ReturnValue(mainsource, "class=\"a-span12 a-color-secondary a-size-base\"", "class=\"a-text-strike\">", "<").Trim().Replace("$", "");
                p.ListPrice = Regex.Replace(p.ListPrice, "<.*?>", string.Empty);
            }

            p.YouSave = "";

            string[] T_c = HtmlHelper.formatString(HtmlHelper.ReturnValue(mainsource, "<title>", "</title>")).Split(':');

            string productNam = HtmlHelper.ReturnValue(mainsource, "id=\"productTitle", ">", "<");
            if (T_c.Length > 1)
                p.ProductCategory = T_c[T_c.Length - 1];


            string ShortContent = HtmlHelper.ReturnValue(mainsource, "<div class=\"a-column a-span6\">", "</table>");

            string Review = HtmlHelper.ReturnValue(ShortContent, "id=\"acrCustomerReviewText\"", ">", "</");
            if (Review == string.Empty)
            {
                Review = HtmlHelper.ReturnValueBefore(ShortContent, "customer reviews", ">");
            }
            if (Review == string.Empty)
            {
                Review = HtmlHelper.ReturnValue(mainsource, "id=\"acrCustomerReviewText", ">", "<");
            }
            Review = Review.Replace("customer reviews", "");
            string Rating = HtmlHelper.ReturnValue(ShortContent, "class=\"a-icon-alt\">", "</").Replace("<span>", "");
            if (Rating.ToLower().Contains("back") || Rating.ToLower().Contains("previous page") || Rating.ToLower().Contains("next page") || Rating.ToLower().Contains("prime") || Rating.ToLower().Contains("next") || Rating.ToLower().Contains("facebook") || Rating.ToLower().Contains("twitter"))
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "<span id=\"acrPopover\"", "title=\"", "\">");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "id=\"reviewStarsLinkedCustomerReviews\"", "alt\">", "</").Replace("<span>", "");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(ShortContent, "out of 5 stars\">", "</").Replace("<span>", "");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "-rating-out-of-text\">", ">", "<");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "<span id=\"acrPopover\"", "title=\"", "\">");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "average_customer_reviews", "span class=\"a-icon-alt\">", "</span>");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "a-icon a-icon-star a-star-4-5", "a-icon-alt\">", "</span>");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "\"arp-rating-out-of-text\">", "</span>");
            }
            if (Rating.Contains("Facebook") || Rating.Contains("Twitter") || Rating.Contains("Pinterest") || Rating.Contains("Back") || Rating.Contains("Prime") || Rating.Contains("Next") || Rating.Contains("Previous page") || Rating.Contains("Next page"))
            {
                Rating = "";
            }

            p.CustomerRating = Rating.Trim() != "" ? Convert.ToDecimal(Rating.Replace(" out of 5 stars", "")) : 0;
            p.CustomerRatingCount = Review.Trim() != "" ? Convert.ToInt32(Review.Replace(",", "").Replace(" ratings", "")) : 0;
            //p.IsPrime = "";

            string AmazonSellerId = string.Empty;
            var sellerDiv = doc.GetElementbyId("merchant-info");
            if (sellerDiv.InnerHtml.Trim().Contains("Ships from and sold by Amazon.com.".Trim()))
            {
                AmazonSellerId = "Amazon.com";
            }
            else
            {
                string sellerInnerstring = sellerDiv.InnerHtml;
                AmazonSellerId = HtmlHelper.ReturnValue(sellerInnerstring, "href=\"", "seller=", "&");
            }
            p.AmazonSellerId = AmazonSellerId;

            if (p.AmazonSellerId == null || p.AmazonSellerId == "")
            {
                p.AmazonSellerId = "NA";
            }


            string amazon_choice = HtmlHelper.formatString(HtmlHelper.ReturnValue(mainsource, "class=\"ac-badge-text-primary\">", "</a>")).Trim().TrimEnd();
            p.IsAmazonChoice = amazon_choice != "" ? true : false;

            var bestSellerRankElement = doc.GetElementbyId("SalesRank");
            string bestSellerRankShortString = string.Empty;
            if (bestSellerRankElement != null)
            {
                bestSellerRankShortString = bestSellerRankElement.InnerHtml;
                string[] stringArray = HtmlHelper.CollectUrl(bestSellerRankShortString, "", "<li", "</li>");
                p.BestSellerRank1 = HtmlHelper.ReturnValue(bestSellerRankShortString, "</b>", "#", " in ").Trim() != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(bestSellerRankShortString, "</b>", "#", " in ").Replace(",", "")) : 0;
                p.BestSellerInCategory1 = HtmlHelper.ReturnValue(bestSellerRankShortString, "</b>", " in ", "(<a href=");
                if (stringArray.Length > 0)
                {
                    p.BestSellerRank2 = HtmlHelper.ReturnValue(stringArray[0], "class=\"zg_hrsr_rank\">", "#", "</span>").Trim() != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(bestSellerRankShortString, "class=\"zg_hrsr_rank\">", "#", "</span>").Replace(",", "")) : 0;
                    p.BestSellerInCategory2 = HtmlHelper.ReturnValue(stringArray[0], "<a href=", ">", "</a>");
                }
                if (stringArray.Length > 1)
                {
                    p.BestSellerRank3 = HtmlHelper.ReturnValue(stringArray[1], "class=\"zg_hrsr_rank\">", "#", "</span>").Trim() != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(bestSellerRankShortString, "class=\"zg_hrsr_rank\">", "#", "</span>").Replace(",", "")) : 0;
                    p.BestSellerInCategory3 = HtmlHelper.ReturnValue(stringArray[1], "<a href=", ">", "</a>");
                }
                if (stringArray.Length > 2)
                {
                    p.BestSellerRank4 = HtmlHelper.ReturnValue(stringArray[2], "class=\"zg_hrsr_rank\">", "#", "</span>").Trim() != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(bestSellerRankShortString, "class=\"zg_hrsr_rank\">", "#", "</span>").Replace(",", "")) : 0;
                    p.BestSellerInCategory4 = HtmlHelper.ReturnValue(stringArray[2], "<a href=", ">", "</a>");
                }

            }
            else
            {
                bestSellerRankShortString = HtmlHelper.ReturnValue(mainsource, "Best Sellers Rank", "<td>", "</td>");
                string[] stringArray = HtmlHelper.CollectUrl(bestSellerRankShortString, "", "<span", "</span>");

                if (stringArray.Length > 0)
                {
                    p.BestSellerRank1 = HtmlHelper.ReturnValue(stringArray[0], ">", "#", " in").Trim() != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(bestSellerRankShortString, ">", "#", " in").Replace(",", "")) : 0;
                    p.BestSellerInCategory1 = HtmlHelper.ReturnValue(stringArray[0], "<a href=", ">", "</a>").Replace("See Top 100 in ", "");
                }
                if (stringArray.Length > 1)
                {
                    p.BestSellerRank2 = HtmlHelper.ReturnValue(stringArray[1], ">", "#", " in").Trim() != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(bestSellerRankShortString, ">", "#", " in").Replace(",", "")) : 0;
                    p.BestSellerInCategory2 = HtmlHelper.ReturnValue(stringArray[1], "<a href=", ">", "</a>");
                }
                if (stringArray.Length > 2)
                {
                    p.BestSellerRank3 = HtmlHelper.ReturnValue(stringArray[2], ">", "#", " in").Trim() != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(bestSellerRankShortString, ">", "#", " in").Replace(",", "")) : 0;
                    p.BestSellerInCategory3 = HtmlHelper.ReturnValue(stringArray[2], "<a href=", ">", "</a>");
                }
                if (stringArray.Length > 3)
                {
                    p.BestSellerRank4 = HtmlHelper.ReturnValue(stringArray[3], ">", "#", " in").Trim() != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(bestSellerRankShortString, ">", "#", " in").Replace(",", "")) : 0;
                    p.BestSellerInCategory4 = HtmlHelper.ReturnValue(stringArray[3], "<a href=", ">", "</a>");
                }

            }
            p.WarrentyAndSupport = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Warranty & Support", "table-padding\">", "</div>"), "<.*?>", string.Empty);
            string shipWeight = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Shipping Weight", "class=\"a-size-base\">", "("), "<.*?>", string.Empty);
            string[] weightstr = shipWeight.Split(' ');
            // var resultString = Regex.Split(shipWeight, @"[^0-9\.]+").Where(c => c != "." && c.Trim() != ""); //Regex.Match(, @"\F+").Value;
            if (weightstr.Length > 0)
            {
                decimal Weight = 0;
                if (weightstr[0].Trim() != "")
                    decimal.TryParse(weightstr[0], out Weight);
                p.ShippingWeight = Weight == 0 ? (decimal?)null : Weight;
            }
            //p.ShippingDetails = "";
            //p.Is10daysReplacement = "";
            //p.Is30daysReplacement = "";
            //p.Is60daysReplacement = "";


            //p.ProductDimensions = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Package Dimensions", "class=\"a-size-base\">", "</td>"), "<.*?>", string.Empty);
            //p.ItemModelNumber = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Item model number", "class=\"a-size-base\">", "</td>"), "<.*?>", string.Empty);
            //p.ItemWeight = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Item Weight", "class=\"a-size-base\">", "</td>"), "<.*?>", string.Empty);

            p.ProductDimensions = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Package Dimensions", ">", "</"), "<.*?>", string.Empty);
            if (p.ProductDimensions.Trim() == "")
            {

                p.ProductDimensions = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Product Dimensions", ">", "</"), "<.*?>", string.Empty);
            }
            p.ItemModelNumber = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Item model number", ">", "</"), "<.*?>", string.Empty);
            p.ItemWeight = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Item Weight", "class=\"a-size-base\">", "</td>"), "<.*?>", string.Empty);
            p.DateFirstListedOnAmazon = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Date First Available", ">", "<"), "<.*?>", string.Empty);

            p.DateFirstListedOnAmazon = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Date First Available", "class=\"a-size-base\">", "</td>"), "<.*?>", string.Empty); ;

            var ele_productFeatureBullets = doc.GetElementbyId("feature-bullets");
            if (ele_productFeatureBullets != null)
            {
                productFeatureBullets(ele_productFeatureBullets.InnerHtml);

            }

        }
        [NonAction]
        public void productFeatureBullets(string bulletString)
        {
            string[] FeatureBulletsArray = HtmlHelper.CollectUrl(bulletString, "", "<li", "</li>");
            for (int i = 0; i < FeatureBulletsArray.Length; i++)
            {
                bool isHiddenOnUI = false;
                if (FeatureBulletsArray[i].Contains("class=\"aok-hidden\""))
                {
                    isHiddenOnUI = true;
                }
                string str= Regex.Replace(HtmlHelper.ReturnValue(FeatureBulletsArray[i], "<span", ">", "</span>"), "<.*?>", string.Empty);
            }
        }

        [NonAction]
        public void productTechnicalDetail(string mainsource, string ASIN, HtmlDocument doc)
        {

            string short_src = HtmlHelper.ReturnValue(mainsource, "id=\"productDetails_detailBullets_sections1\"", "</table>");
            string[] stringArray = HtmlHelper.CollectUrl(short_src, "", "<tr>", "</tr>");
            List<TechnicalDetail> lstTechdetail = new List<DB.TechnicalDetail>();
            for (int i = 0; i < stringArray.Length; i++)
            {
                TechnicalDetail techdetail = new DB.TechnicalDetail();
                techdetail.ASIN = ASIN;
                techdetail.Attribute = HtmlHelper.ReturnValue(stringArray[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>");
                if (HtmlHelper.ReturnValue(stringArray[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>").Trim() == "Customer Reviews".Trim())
                {
                    techdetail.Value = HtmlHelper.ReturnValue(stringArray[i], "id=\"acrPopover\"", "title=\"", "\"") + " - " + HtmlHelper.ReturnValue(stringArray[i], "id=\"acrCustomerReviewText\"", ">", "</span>");
                }
                else
                {
                    techdetail.Value = Regex.Replace(HtmlHelper.ReturnValue(stringArray[i], "<td", ">", "</td>"), "<.*?>", string.Empty);
                }
                lstTechdetail.Add(techdetail);
            }

            var ele_techSpec_section_1 = doc.GetElementbyId("productDetails_techSpec_section_1");
            if (ele_techSpec_section_1 != null)
            {
                string techSpec_section_1 = ele_techSpec_section_1.InnerHtml;
                string[] techSpec_section_1Array = HtmlHelper.CollectUrl(techSpec_section_1, "", "<tr>", "</tr>");

                for (int i = 0; i < techSpec_section_1Array.Length; i++)
                {
                    TechnicalDetail techdetail = new DB.TechnicalDetail();
                    techdetail.ASIN = ASIN;
                    techdetail.Attribute = HtmlHelper.ReturnValue(techSpec_section_1Array[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>");
                    if (HtmlHelper.ReturnValue(techSpec_section_1Array[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>").Trim() == "Customer Reviews".Trim())
                    {
                        techdetail.Value = HtmlHelper.ReturnValue(techSpec_section_1Array[i], "id=\"acrPopover\"", "title=\"", "\"") + " - " + HtmlHelper.ReturnValue(stringArray[i], "id=\"acrCustomerReviewText\"", ">", "</span>");
                    }
                    else
                    {
                        techdetail.Value = Regex.Replace(HtmlHelper.ReturnValue(techSpec_section_1Array[i], "<td", ">", "</td>"), "<.*?>", string.Empty);
                    }
                    lstTechdetail.Add(techdetail);
                }
            }
            ///////////////////
            var ele_techSpec_section_2 = doc.GetElementbyId("productDetails_techSpec_section_2");
            if (ele_techSpec_section_2 != null)
            {
                string techSpec_section_2 = ele_techSpec_section_2.InnerHtml;
                string[] techSpec_section_2Array = HtmlHelper.CollectUrl(techSpec_section_2, "", "<tr>", "</tr>");

                for (int i = 0; i < techSpec_section_2Array.Length; i++)
                {
                    TechnicalDetail techdetail = new DB.TechnicalDetail();
                    techdetail.ASIN = ASIN;
                    techdetail.Attribute = HtmlHelper.ReturnValue(techSpec_section_2Array[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>");
                    if (HtmlHelper.ReturnValue(techSpec_section_2Array[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>").Trim() == "Customer Reviews".Trim())
                    {
                        techdetail.Value = HtmlHelper.ReturnValue(techSpec_section_2Array[i], "id=\"acrPopover\"", "title=\"", "\"") + " - " + HtmlHelper.ReturnValue(stringArray[i], "id=\"acrCustomerReviewText\"", ">", "</span>");
                    }
                    else
                    {
                        techdetail.Value = Regex.Replace(HtmlHelper.ReturnValue(techSpec_section_2Array[i], "<td", ">", "</td>"), "<.*?>", string.Empty);
                    }
                    lstTechdetail.Add(techdetail);
                }
            }
            p.TechnicalDetails = lstTechdetail;
        }
        [NonAction]
        public void productByFeatureDetail(string mainsource, string ASIN)
        {
            System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("<[^>]*>");
            string short_src = HtmlHelper.ReturnValue(mainsource, "id=\"cr-summarization-attributes-list\"", "<hr class=\"a-spacing-large a-spacing-top-extra-large a-divider-normal\">");

            string[] stringArray = mainsource.Split(new string[] { "id=\"cr-summarization-attributes-list\"" }, StringSplitOptions.None);


            List<TechnicalDetail> lstTechdetail = new List<DB.TechnicalDetail>();
            for (int i = 0; i < stringArray.Length; i++)
            {
                TechnicalDetail techdetail = new DB.TechnicalDetail();
                techdetail.ASIN = ASIN;
                techdetail.Attribute = HtmlHelper.ReturnValue(stringArray[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>");
                if (HtmlHelper.ReturnValue(stringArray[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>").Trim() == "Customer Reviews".Trim())
                {
                    techdetail.Value = HtmlHelper.ReturnValue(stringArray[i], "id=\"acrPopover\"", "title=\"", "\"") + " - " + HtmlHelper.ReturnValue(stringArray[i], "id=\"acrCustomerReviewText\"", ">", "</span>");
                }
                else
                {
                    techdetail.Value = Regex.Replace(HtmlHelper.ReturnValue(stringArray[i], "<td", ">", "</td>"), "<.*?>", string.Empty);
                }
                lstTechdetail.Add(techdetail);
            }
        }
        #endregion
        #region PRODUCT SELLER DATA
        [NonAction]
        public void getproductSellerMapping(string ASIN)
        {
            List<Seller> lstSeller = new List<DB.Seller>();

            int sellerPageCout = 0;
            int pagesize = 1;
            int pageCount = 2;

            List<ProductSellerMapping> lstproductsellermapping = new List<ProductSellerMapping>();
            while (pageCount != pagesize)
            {
                int retry = Convert.ToInt32(ConfigurationManager.AppSettings["_retry"]);
                sellerSource_retry:
                var sellerSource = GetSource("https://www.amazon.com/gp/offer-listing/" + ASIN + "/ref=olp_page_" + pagesize + "?ie=UTF8&f_all=true&startIndex=" + sellerPageCout + "", "");

                #region RETRY if Source is Empty
                if (sellerSource == "" && retry < 5)
                {
                    retry++;

                    goto sellerSource_retry;
                }
                else if (sellerSource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
                {
                    retry++;

                    goto sellerSource_retry;
                }
                #endregion

                string[] stringArray = sellerSource.Split(new string[] { "class=\"a-row a-spacing-mini olpOffer\"" }, StringSplitOptions.None);


                for (int j = 1; j < stringArray.Length; j++)
                {
                    Seller seller = new Seller();
                    string isAmazonFulfilled = HtmlHelper.ReturnValue(stringArray[j], "class=\"a-size-medium a-text-bold\"", "isAmazonFulfilled=", "&");
                    string sellerID = HtmlHelper.ReturnValue(stringArray[j], "class=\"a-size-medium a-text-bold\"", ";seller=", "\"");
                    string sellerUrl = "https://www.amazon.com/sp?_encoding=UTF8&isAmazonFulfilled=" + isAmazonFulfilled + "&seller=" + sellerID;

                    ProductSellerMapping productsellermapping = new ProductSellerMapping();
                    productsellermapping.ASIN = ASIN;
                    productsellermapping.Condition = HtmlHelper.ReturnValue(stringArray[j], "class=\"a-size-medium olpCondition a-text-bold\"", ">", "</span>");
                    productsellermapping.Price = HtmlHelper.ReturnValue(stringArray[j], "class=\"a-size-large a-color-price olpOfferPrice a-text-bold\"", ">", "</span>");
                    productsellermapping.AmazonSellerId = sellerID;
                    lstproductsellermapping.Add(productsellermapping);
                    if (productsellermapping.AmazonSellerId != "")
                    {
                        seller = getSellerDetail(sellerUrl);
                        seller.AmazonSellerId = sellerID;
                        lstSeller.Add(seller);
                    }
                    else
                    {
                        productsellermapping.AmazonSellerId = HtmlHelper.ReturnValue(stringArray[j], "<img", "alt=\"", "\"");
                    }
                    //seller.ProductSellerMappings = lstproductsellermapping;

                }

                if (sellerPageCout == 0)
                {
                    string short_src = HtmlHelper.ReturnValue(sellerSource, "class=\"a-pagination\"", "</ul>");
                    string[] pagingArray = short_src.Split(new string[] { "</li>" }, StringSplitOptions.None);
                    pageCount = pagingArray.Length - 3;
                    if (pageCount < 1)
                        break;
                }
                pagesize++;
                sellerPageCout = sellerPageCout + 10;
            }


            using (DBModels context = new DBModels())
            {
                context.BulkInsert(lstSeller, options =>
                {
                    options.InsertIfNotExists = true;
                    options.AllowUpdatePrimaryKeys = true;
                });
            }
            //using (DBModels context = new DBModels())
            //{
            //    context.BulkInsert(lstproductsellermapping, options =>
            //    {
            //        options.InsertIfNotExists = true;
            //        options.AllowUpdatePrimaryKeys = true;
            //    });
            //}
            p.ProductSellerMappings = lstproductsellermapping;
            // return lstSeller;
        }
        public Seller getSellerDetail(string sellerprofileUrl)
        {
            Seller seller = new DB.Seller();

            int retry = Convert.ToInt32(ConfigurationManager.AppSettings["_retry"]);
            sellerProfileSource_retry:
            var sellerSource = GetSource(sellerprofileUrl, "");
            #region RETRY if Source is Empty
            if (sellerSource == "" && retry < 5)
            {
                retry++;

                goto sellerProfileSource_retry;
            }
            else if (sellerSource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
            {
                retry++;

                goto sellerProfileSource_retry;
            }
            #endregion

            seller.SellerName = HtmlHelper.ReturnValue(sellerSource, "id=\"sellerName\"", ">", "<");
            seller.SellerFrontURL = sellerprofileUrl;
            seller.SellerShopURL = "https://www.amazon.com" + HtmlHelper.ReturnValue(sellerSource, "id=\"products-link\"", "href=\"", "\">Products</a>");
            seller.ProfileImageURL = HtmlHelper.ReturnValue(sellerSource, "id=\"seller-logo-container\"", "src=\"", "\"");

            seller.Rating = HtmlHelper.ReturnValue(sellerSource, "class=\"a-link-normal feedback-detail-description\"", "(", " ratings)");
            seller.WhyChooseUs = HtmlHelper.ReturnValue(sellerSource, "id=\"about-seller-expanded\"", ">", "</span>");
            seller.CustomerServicePhone = "";

            string short_src = HtmlHelper.ReturnValue(sellerSource, "id=\"feedback-summary-table\"", "</table>");
            string[] stringArray = HtmlHelper.CollectUrl(short_src, "", "<tr>", "</tr>");
            for (int i = 1; i < stringArray.Length; i++)
            {
                string[] feedbackArray = stringArray[i].Split(new string[] { "</td>" }, StringSplitOptions.None);
                if (feedbackArray.Length > 4)
                {
                    if (feedbackArray[0].Contains("Positive"))
                    {
                        string days30 = HtmlHelper.ReturnValue(feedbackArray[1].Replace("-", "").Replace(" ", ""), " < span", ">", "</span>").Replace(",", "");
                        string days90 = HtmlHelper.ReturnValue(feedbackArray[2].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string month12 = HtmlHelper.ReturnValue(feedbackArray[3].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string LifeTime = HtmlHelper.ReturnValue(feedbackArray[4].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        seller.Positive_30days = Convert.ToInt32(days30 != "" ? days30 : null);
                        seller.Positive_90days = Convert.ToInt32(days90 != "" ? days90 : null);
                        seller.Positive_12Month = Convert.ToInt32(month12 != "" ? month12 : null);
                        seller.Positive_LifeTime = Convert.ToInt32(LifeTime != "" ? LifeTime : null);

                    }

                    if (feedbackArray[0].Contains("Neutral"))
                    {
                        string days30 = HtmlHelper.ReturnValue(feedbackArray[1].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string days90 = HtmlHelper.ReturnValue(feedbackArray[2].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string month12 = HtmlHelper.ReturnValue(feedbackArray[3].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string LifeTime = HtmlHelper.ReturnValue(feedbackArray[4].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        seller.Neutral_30days = Convert.ToInt32(days30 != "" ? days30 : null);
                        seller.Neutral_90days = Convert.ToInt32(days90 != "" ? days90 : null);
                        seller.Neutral_12Month = Convert.ToInt32(month12 != "" ? month12 : null);
                        seller.Neutral_LifeTime = Convert.ToInt32(LifeTime != "" ? LifeTime : null);

                    }
                    if (feedbackArray[0].Contains("Negative"))
                    {
                        string days30 = HtmlHelper.ReturnValue(feedbackArray[1].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string days90 = HtmlHelper.ReturnValue(feedbackArray[2].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string month12 = HtmlHelper.ReturnValue(feedbackArray[3].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string LifeTime = HtmlHelper.ReturnValue(feedbackArray[4].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        seller.Negative_30days = Convert.ToInt32(days30 != "" ? days30 : null);
                        seller.Negative_90days = Convert.ToInt32(days90 != "" ? days90 : null);
                        seller.Negative_12Month = Convert.ToInt32(month12 != "" ? month12 : null);
                        seller.Negative_LifeTime = Convert.ToInt32(LifeTime != "" ? LifeTime : null);

                    }
                }

            }

            return seller;
        }
        #endregion
        #region PRODUCT REVIEW DATA
        [NonAction]
        public void getCutomerReview(string url, string ASIN)
        {
            int reviewPageCout = 1;
            int pageCount = 2;
            List<CustomerReview> lstCutomerReview = new List<CustomerReview>();
            List<Customer> lstCutomer = new List<Customer>();
            while (reviewPageCout <= pageCount)
            {
                int retry = Convert.ToInt32(ConfigurationManager.AppSettings["_retry"]);
                ReviewSource_retry:
                var ReviewSource = GetSource("https://www.amazon.com" + url + "&pageNumber=" + reviewPageCout + "&pageSize=20", "");

                #region RETRY if Source is Empty
                if (ReviewSource == "" && retry < 5)
                {
                    retry++;

                    goto ReviewSource_retry;
                }
                else if (ReviewSource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
                {
                    retry++;

                    goto ReviewSource_retry;
                }
                #endregion


                string count = HtmlHelper.ReturnValue(ReviewSource, "Showing 1-20 of", " ", " reviews");
                if (reviewPageCout == 1)
                {
                    pageCount = Convert.ToInt32(Math.Round(0.5 + (Convert.ToDouble(count) / 20)));
                }
                reviewPageCout++;
                #region

                string[] stringArray = HtmlHelper.CollectUrl(ReviewSource, "", "data-hook=\"review\"", "</div></div></div></div></div></div>");

                for (int j = 0; j < stringArray.Length; j++)
                {
                    CustomerReview cutomerreview = new CustomerReview();
                    // cutomerreview.Id
                    string customerName = HtmlHelper.ReturnValue(stringArray[j], "class=\"a-profile-name\"", ">", "<");
                    decimal rating = Convert.ToDecimal(HtmlHelper.ReturnValue(stringArray[j], "class=\"a-link-normal\"", "title=\"", " out of 5 stars").Trim() != "" ? HtmlHelper.ReturnValue(stringArray[j], "class=\"a-link-normal\"", "title=\"", " out of 5 stars").Replace(",", "").Trim() : "0.0");
                    cutomerreview.ASIN = ASIN;
                    cutomerreview.AmazonReviewId = HtmlHelper.ReturnValue(stringArray[j], "<div id=\"customer_review", "-", "\" class=");
                    cutomerreview.ReviewTitle = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"review-title\"", "<span class=\"cr-original-review-content\">", "</span>");
                    if (cutomerreview.ReviewTitle.Trim() == "" || cutomerreview.ReviewTitle.Trim() == null)
                    {
                        cutomerreview.ReviewTitle = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"review-title\"", "<span>", "</span>");
                    }
                    cutomerreview.ReviewDate = Convert.ToDateTime(HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"review-date\"", ">", "<").Trim());
                    cutomerreview.ReviewRating = rating;//Convert.ToDecimal(HtmlHelper.ReturnValue(stringArray[j], "class=\"a-link-normal\"", "title=\"", " out of 5 stars"));

                    cutomerreview.Description = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"review-body\"", "<span class=\"cr-original-review-content\">", "</span>");
                    if (cutomerreview.Description == "" || cutomerreview.Description == null)
                    {
                        cutomerreview.Description = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"review-body\"", "<span>", "</span>");
                    }
                    cutomerreview.Color = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"format-strip\"", "Color: ", "</a>");
                    cutomerreview.PeopleHelpfulCount = Convert.ToInt32(HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"helpful-vote-statement\"", ">", "people found this helpful").Trim() == "" ? null : HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"helpful-vote-statement\"", ">", "people found this helpful").Replace(",", "").Trim());
                    cutomerreview.IsVerifiedPurchase = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"avp-badge\"", ">", "<") != "" ? true : false;
                    cutomerreview.IsPositiveReview = rating > 3 ? true : false;
                    string CustomerLink = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"genome-widget\"", "href=\"", "\"");

                    string[] splitUrl = CustomerLink.Split('/');
                    //cutomerreview.ReviewedBy = HtmlHelper.ReturnValue(stringArray[j], "href=\"/gp/profile", "/", "/ref=");
                    cutomerreview.ReviewedBy = splitUrl[3];


                    lstCutomerReview.Add(cutomerreview);


                    if (lstCutomer.Any(x => x.AmazonUserId == cutomerreview.ReviewedBy))
                    {

                    }
                    else
                    {
                        Customer customer = new Customer();
                        //cutomerreview.Customer = getCustomerDetail(CustomerLink, ASIN, customerName);
                        customer = getCustomerDetail(CustomerLink, ASIN, customerName);
                        lstCutomer.Add(customer);
                    }
                }

                #endregion

            }

            try
            {
                using (DBModels context = new DBModels())
                {
                    context.BulkInsert(lstCutomer, options =>
                    {
                        options.InsertIfNotExists = true;
                        options.AllowUpdatePrimaryKeys = true;
                    });
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            //using (DBModels context = new DBModels())
            //{
            //    context.BulkInsert(lstCutomerReview, options =>
            //    {
            //        options.InsertIfNotExists = true;
            //        options.AllowUpdatePrimaryKeys = true;
            //    });
            //}
            p.CustomerReviews = lstCutomerReview;
            // return lstCutomerReview;
        }
        [NonAction]//THIS METHOD WILL BE CALLED AT THE TIME OF GETTING CUSTOMER REVIEW
        public Customer getCustomerDetail(string url, string ASIN, string customerName)
        {
            string[] splitUrl = url.Split('/');

            string boiUrl = string.Empty;
            string dashBoardUrl = string.Empty;
            string profileImageUrl = string.Empty;
            string imazonUserId = string.Empty;
            if (splitUrl[3].Contains("amzn1.account"))
            {
                boiUrl = "https://www.amazon.com/profilewidget/bio/" + splitUrl[3] + "?view=visitor";
                dashBoardUrl = "https://www.amazon.com/hz/gamification/api/contributor/dashboard/" + splitUrl[3] + "?ownerView=false&customerFollowEnabled=false";
                profileImageUrl = "https://www.amazon.com/avatar/default.json?customer_id=" + splitUrl[3];
                imazonUserId = splitUrl[3];
            }
            else
            {
                return null;
            }

            var bioSource = GetSource(boiUrl, "");
            var dashBoarSource = GetSource(dashBoardUrl, "");
            var profileImageSource = GetSource(profileImageUrl, "");

            //string CustomerSource = GetSource("https://www.amazon.com" + url, "");

            Customer cust = new Customer();
            cust.AmazonUserId = imazonUserId;//HtmlHelper.ReturnValue(url, "account", ".", "/ref");
            cust.CustomerFrontURL = "https://www.amazon.com" + url;
            cust.ProfileImageURL = HtmlHelper.ReturnValue(profileImageSource, "\"url\":", "\"", "\",");
            //"nameHeaderData":{ "name":"Kindle Customer",
            cust.Name = customerName;
            cust.Designation = HtmlHelper.ReturnValue(bioSource, "\"occupation\":", "\"", "\",");
            cust.About = HtmlHelper.ReturnValue(bioSource, "\"personalDescription\"", ":", ",\"showAboutMe\"");

            string ReviewRanking = HtmlHelper.ReturnValue(bioSource, "\"topReviewerInfo\":", "\"rank\":", ",\"decoratedRank").Replace("null", "");
            cust.ReviewRanking = long.Parse(ReviewRanking == "" ? "0" : ReviewRanking);


            string HelpfulVotes = HtmlHelper.ReturnValue(dashBoarSource, "\"helpfulVotesData\":", "\"count\":\"", "\",").Replace(",", "");
            cust.HelpfulVotes = Convert.ToInt32(HelpfulVotes == "" ? "0" : HelpfulVotes);
            string Review = HtmlHelper.ReturnValue(dashBoarSource, "\"reviewsCountData\":", "\"count\":\"", "\",").Replace(",", "");
            cust.Reviews = Convert.ToInt32(Review == "" ? "0" : Review);
            string Hearts = HtmlHelper.ReturnValue(dashBoarSource, "\"ideaListHeartsData\":", "\"count\":\"", "\",").Replace(",", "");
            cust.Hearts = Convert.ToInt32(Hearts == "" ? "0" : Hearts);
            string IdeaLists = HtmlHelper.ReturnValue(dashBoarSource, "\"ideaListData\":", "\"count\":\"", "\",").Replace(",", "");
            cust.IdeaLists = Convert.ToInt32(IdeaLists == "" ? "0" : IdeaLists);

            return cust;
        }
        #endregion
        #region PRODUCT QUESTION AND ANSWER
        [NonAction]
        public void getFAQ(string ASIN)
        {
            int faqPageCout = 1;
            int pageCount = 2;
            List<ProductQuestion> lstProductQuestion = new List<ProductQuestion>();
            while (faqPageCout <= pageCount)
            {
                string url = "https://www.amazon.com/ask/questions/asin/" + ASIN + "/";

                int retry = Convert.ToInt32(ConfigurationManager.AppSettings["_retry"]);
                FAQSource_retry:
                var FAQSource = GetSource(url + Convert.ToString(faqPageCout), "");

                #region RETRY if Source is Empty
                if (FAQSource == "" && retry < 5)
                {
                    retry++;

                    goto FAQSource_retry;
                }
                else if (FAQSource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
                {
                    retry++;

                    goto FAQSource_retry;
                }
                #endregion

                string[] stringArray = HtmlHelper.CollectUrl(FAQSource, "", "class=\"a-fixed-left-grid a-spacing-base\"", "</div></div></div></div></div>");
                for (int j = 0; j < stringArray.Length; j++)
                {
                    ProductQuestion productquestion = new ProductQuestion();

                    // cutomerreview.Id
                    productquestion.ASIN = ASIN;
                    productquestion.Question = HtmlHelper.ReturnValue(stringArray[j], "data-ask-no-op=\"{&quot;metricName&quot;:&quot;top-question-text-click&quot;}\"", ">", "<");
                    productquestion.QuestionId = HtmlHelper.ReturnValue(stringArray[j], "class=\"up\"", "action=\"/ask/vote/question/", "\">");
                    productquestion.NumberOfVote = Convert.ToInt32(HtmlHelper.ReturnValue(stringArray[j], "class=\"count\"", ">", "<").Replace(",", ""));

                    string allAnsLink = HtmlHelper.ReturnValue(stringArray[j], "id=\"askSeeAllAnswersLink", "href=\"", "\">");

                    List<QAMapping> lstQAMapping = new List<QAMapping>();

                    if (allAnsLink.Trim() != "")
                    {
                        int faqPageCoutAns = 1;
                        int pageCountAns = 2;
                        allAnsLink = allAnsLink.Replace("ref", "{0}/ref");
                        while (faqPageCoutAns <= pageCountAns)
                        {
                            int retryAns = 0;
                            retryAns:
                            var FAQAnsSource = GetSource("https://www.amazon.com" + string.Format(allAnsLink, faqPageCoutAns), "");
                            #region RETRY if Source is Empty
                            if (FAQAnsSource == "" && retryAns < 5)
                            {
                                retryAns++;
                                Thread.Sleep(1000);
                                goto retryAns;
                            }
                            else if (FAQAnsSource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
                            {
                                retryAns++;
                                Thread.Sleep(1000);
                                goto retryAns;
                            }
                            #endregion

                            if (faqPageCoutAns == 1)
                            {
                                //string urlFaq = HtmlHelper.ReturnValue(FAQSource, "<li class=\"a-disabled\">...</li>", "href=\"", "<");
                                string pageSize = HtmlHelper.ReturnValue(FAQAnsSource, "class=\"a-spacing-large a-color-secondary\"", "of", "answers");
                                pageCountAns = Convert.ToInt32(Math.Round(0.5 + (Convert.ToDouble(pageSize) / 10)));
                                //pageCount = Convert.ToInt32(urlFaq.Substring(urlFaq.LastIndexOf('>') + 1));
                            }

                            string[] FAQansArray = FAQAnsSource.Split(new string[] { "id=\"answer-" }, StringSplitOptions.None); //HtmlHelper.CollectUrl(FAQAnsSource, "", "id=\"answer-", "id=\"askPaginationBar\"");

                            for (int k = 1; k < FAQansArray.Length; k++)
                            {
                                QAMapping qamapping = new QAMapping();

                                qamapping.Answer = HtmlHelper.ReturnValue(FAQansArray[k], "class=\"a-section\"", "<span>", "</span>");
                                qamapping.AnsweredBy = HtmlHelper.ReturnValue(FAQansArray[k], "href=\"/gp/profile", "/", "/ref=");

                                lstQAMapping.Add(qamapping);
                            }
                            faqPageCoutAns++;
                        }
                    }
                    else
                    {
                        QAMapping qamapping = new QAMapping();

                        qamapping.Answer = HtmlHelper.ReturnValue(stringArray[j], "class=\"a-fixed-left-grid-col a-col-right\"", "<span>", "</span>");
                        qamapping.AnsweredBy = HtmlHelper.ReturnValue(stringArray[j], "href=\"/gp/profile", "/", "/ref=");

                        lstQAMapping.Add(qamapping);
                    }

                    productquestion.QAMappings = lstQAMapping;
                    lstProductQuestion.Add(productquestion);
                }




                if (faqPageCout == 1)
                {
                    //string urlFaq = HtmlHelper.ReturnValue(FAQSource, "<li class=\"a-disabled\">...</li>", "href=\"", "<");
                    string tottalPage = HtmlHelper.ReturnValue(FAQSource, "class=\"a-section askPaginationHeaderMessage\"", "of", "questions");
                    pageCount = Convert.ToInt32(Math.Round(0.5 + (Convert.ToDouble(tottalPage) / 10)));
                    //pageCount = Convert.ToInt32(urlFaq.Substring(urlFaq.LastIndexOf('>') + 1));
                }
                faqPageCout++;
            }

            //using (DBModels context = new DBModels())
            //{
            //    context.BulkInsert(lstProductQuestion, options =>
            //    {
            //        options.InsertIfNotExists = true;
            //        options.AllowUpdatePrimaryKeys = true;
            //    });
            //}

            p.ProductQuestions = lstProductQuestion;
            //return lstProductQuestion;
        }
        #endregion

        #region GETSOURCE AND PROXY SETTINGS
        [NonAction]
        public string GetSource(string url, string scookie)
        {
            try
            {
                System.Net.ServicePointManager.Expect100Continue = false;
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AllwaysGoodCertificate);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                HttpWebRequest myWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                myWebRequest.KeepAlive = true;
                myWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                myWebRequest.Method = WebRequestMethods.Http.Get;
                myWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36";
                myWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                myWebRequest.Headers["Accept-Language"] = "en-US,en;q=0.9";
                myWebRequest.Headers["accept-encoding"] = "gzip, deflate, br";
                myWebRequest.Host = "www.amazon.com";
                // myWebRequest.Headers["Cookie"] = "PHPSESSID=fp9ed5gobi6nk1bv0jq59fs4di; StickySession=id.65889726442.546www.investing.com";
                myWebRequest.Headers["X-Requested-With"] = "XMLHttpRequest";

                if (Convert.ToBoolean(ConfigurationSettings.AppSettings["_proxy"].Trim()))
                {
                    myWebRequest.Proxy = SetProxySevere(++ProxyChange);
                    if (ProxyChange >= 16)
                        ProxyChange = 0;
                }
                myWebRequest.Timeout = Timeout.Infinite;
                myWebRequest.AllowAutoRedirect = false;

                HttpWebResponse myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                Stream responseStream = myWebResponse.GetResponseStream();
                StreamReader myWebSource = new StreamReader(responseStream);
                string myPageSource = myWebSource.ReadToEnd();
                myWebResponse.Close();
                responseStream.Close();

                return HtmlHelper.ReturnFormatedHtml(myPageSource);
            }
            catch (Exception ex)
            {
                if (ex.Message.ToString() == "The remote server returned an error: (400) Bad Request.")
                {
                    ////MessageBox.Show("There is an error :"+Environment.NewLine+"Message: "+ex.Message.ToString()+Environment.NewLine+"For Zip=: " +zipcode+", SoreId=: "+StoreId+Environment.NewLine+"And URL=: "+url);
                    //DialogResult da = MessageBox.Show("!!!!  Site Block  !!!!!\nWant to continue downloading????", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    //if (DialogResult.Yes == da)
                    //{
                    //    MessageBox.Show("Firstly change Proxy after that click OK");
                    //}
                    //else if (DialogResult.No == da)
                    //{
                    //    Environment.Exit(-1);
                    //}
                }
                //            	return "The remote server returned an error: (400) Bad Request.";
                return string.Empty;
            }
        }
        [NonAction]
        public WebProxy SetProxySevere(int ChangeCount)
        {
            WebProxy setproxy = new WebProxy("world.proxymesh.com", 31280);
            //if (ChangeCount <= 4)
            //{
            //    setproxy = new WebProxy("us.proxymesh.com", 31280);
            //}
            //else
            //{
            //    if (ChangeCount <= 8)
            //    {
            //        setproxy = new WebProxy("us-il.proxymesh.com", 31280);
            //    }
            //    else
            //    {
            //        if (ChangeCount <= 12)
            //        {
            //            setproxy = new WebProxy("us-fl.proxymesh.com", 31280);
            //        }
            //        else
            //        {
            //            if (ChangeCount <= 16)
            //            {
            //                setproxy = new WebProxy("us-ca.proxymesh.com", 31280);
            //            }
            //            else
            //            {
            //                setproxy = new WebProxy("us-dc.proxymesh.com", 31280);
            //                ChangeCount = 0;
            //            }
            //        }
            //    }

            //}
            return setproxy;
        }
        [NonAction]
        private static bool AllwaysGoodCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
        {
            return true;

        }
        #endregion

    }
}
