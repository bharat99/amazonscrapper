﻿using AmazoneASINScrapper.BAL.helper;
using AmazoneASINScrapper.BAL.Models;
using AmazoneASINScrapper.DAL.DBEntity;
using AmazoneASINScrapper.helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazoneASINScrapper.BAL.BusinessRepo
{
    public class SellerProductRepo
    {
        public notification saveScrapedDetail(ProductDetail p, List<Customer> lstCust, List<Seller> lstSeller)
        {
            notification savenotification = new Models.notification();
            bool custSave = saveCustomer(lstCust);
            
            #region SAVE PRODUCT INFORMATION
            int affectedDatas = 0;
            int retry = 0;
        ProductInsert_retry:
            using (AmazonDB context = new AmazonDB())
            {
                try
                {
                    #region Product Procedure
                    
                    affectedDatas = context.Database.ExecuteSqlCommand("exec spSaveSellerProduct @ASIN,@Title,@Description,@ImageLink,@ImageLink1,@ImageLink2,@ImageLink3,"
                                                      + "@ImageLink4,@ImageLink5,@ImageLink6,@VideoLink,@Price,@ListPrice,@YouSave,@ProductCategory,@ProductPosition,"
                                                      + "@CustomerRating,@CustomerRatingCount,@IsPrime,@AmazonSellerId,@IsAmazonChoice,"
                                                      + "@WarrentyAndSupport,@ShippingWeight,@ShippingDetails,@Is10daysReplacement,@Is30daysReplacement,@Is60daysReplacement,"
                                                      + "@ProductDimensions,@ItemModelNumber,@ItemWeight,@DateFirstListedOnAmazon,@IsInStock,@IsAplusContent,@MarketPlaceID",
                                                    new SqlParameter("@ASIN", p.ASIN),
                                                    new SqlParameter("@Title", p.Title),
                                                    new SqlParameter("@Description", p.Description ?? ""),
                                                    new SqlParameter("@ImageLink", p.ImageLink ?? ""),
                                                    new SqlParameter("@ImageLink1", p.ImageLink1 ?? ""),
                                                    new SqlParameter("@ImageLink2", p.ImageLink2 ?? ""),
                                                    new SqlParameter("@ImageLink3", p.ImageLink3 ?? ""),
                                                    new SqlParameter("@ImageLink4", p.ImageLink4 ?? ""),
                                                    new SqlParameter("@ImageLink5", p.ImageLink5 ?? ""),
                                                    new SqlParameter("@ImageLink6", p.ImageLink6 ?? ""),
                                                    new SqlParameter("@VideoLink", p.VideoLink ?? ""),
                                                    new SqlParameter("@Price", p.Price ?? ""),
                                                    new SqlParameter("@ListPrice", p.ListPrice ?? ""),
                                                    new SqlParameter("@YouSave", p.YouSave ?? ""),
                                                    new SqlParameter("@ProductCategory", p.ProductCategory ?? ""),
                                                    new SqlParameter("@ProductPosition", (object)p.ProductPosition ?? DBNull.Value),
                                                    new SqlParameter("@CustomerRating", (object)p.CustomerRating ?? DBNull.Value),
                                                    new SqlParameter("@CustomerRatingCount", (object)p.CustomerRatingCount ?? DBNull.Value),
                                                    new SqlParameter("@IsPrime", p.IsPrime ?? false),
                                                    new SqlParameter("@AmazonSellerId", p.AmazonSellerId ?? ""),
                                                    new SqlParameter("@IsAmazonChoice", p.IsAmazonChoice ?? false),
                                                    new SqlParameter("@WarrentyAndSupport", p.WarrentyAndSupport ?? ""),
                                                    new SqlParameter("@ShippingWeight", (object)p.ShippingWeight ?? DBNull.Value),
                                                    new SqlParameter("@ShippingDetails", p.ShippingDetails ?? ""),
                                                    new SqlParameter("@Is10daysReplacement", p.Is10daysReplacement ?? false),
                                                    new SqlParameter("@Is30daysReplacement", p.Is30daysReplacement ?? false),
                                                    new SqlParameter("@Is60daysReplacement", p.Is60daysReplacement ?? false),
                                                    new SqlParameter("@ProductDimensions", p.ProductDimensions ?? ""),
                                                    new SqlParameter("@ItemModelNumber", p.ItemModelNumber ?? ""),
                                                    new SqlParameter("@ItemWeight", p.ItemWeight ?? ""),
                                                    new SqlParameter("@DateFirstListedOnAmazon", p.DateFirstListedOnAmazon ?? ""),
                                                    new SqlParameter("@IsInStock", p.IsInStock ?? false),
                                                    new SqlParameter("@IsAplusContent", p.IsAplusContent ?? false),
                                                    new SqlParameter("@MarketPlaceID", p.MarketPlaceID ?? "")
                                                    );
                    #endregion
                    if (affectedDatas <= 0 && retry < Convert.ToInt32(ConfigurationSettings.AppSettings["_retryDB"]))
                    {
                        retry++;

                        goto ProductInsert_retry;
                    }
                }
                catch (Exception ex)
                {

                    ExceptionHandling.CreateException(ex, "Question Answer: " + "saveScrapedDetail", new Dictionary<string, object>() { { p.ASIN, p } });
                }
                context.SaveChanges();
            }
            #endregion

            if (affectedDatas > 0)
            {

                #region SAVE Product Review
                bool customerReviewSave = saveCustomerReview(p.CustomerReviews.ToList());

                #endregion

                #region SAVE Product QnA
                bool qaSave = saveQnA(p.ProductQuestions.ToList());

                #endregion


                #region SAVE Product BSR
                bool bsrSave = saveBSRs(p.ProductBSRs.ToList());

                #endregion

                #region SAVE Product Key Feature
                bool keyfeatureSave = saveKeyFearture(p.ProductKeyFeatures.ToList());

                #endregion

                #region SAVE BYFEATURE
                bool byFeatureSave = saveByFeature(p.ByFeatures.ToList());
                #endregion

                #region SAVE CUSTOMER INETEREST GROUP
                bool customerGrpSave = saveCustomerGroupsInterest(p.CustomerGroupsInterests.ToList());
                #endregion

                #region SAVE TECHNICAL DETAIL
                bool techInfoSave = saveTechnicalDetail(p.TechnicalDetails.ToList());
                #endregion

                savenotification.customer = custSave;
                savenotification.product = true;
                savenotification.CustomerReview = customerReviewSave;
                savenotification.questionAns = qaSave;
                savenotification.bestSellerRank = bsrSave;
                savenotification.KeyFearture = keyfeatureSave;
                savenotification.ByFeature = byFeatureSave;
                savenotification.CustomerGroupsInterest = customerGrpSave;
                savenotification.TechnicalDetail = techInfoSave;

            }
            using (AmazonDB context = new AmazonDB())
            {
                try
                {
                    var record = context.ASINs.Where(x => x.ASIN1 == p.ASIN).FirstOrDefault();
                    if (record != null)
                    {
                        record.Status = "Executed";
                        record.EndOn = DateTime.Now;
                        record.customer = savenotification.customer;
                        
                        record.product = savenotification.product;
                        record.CustomerReview = savenotification.CustomerReview;
                        record.questionAns = savenotification.questionAns;
                        
                        record.bestSellerRank = savenotification.bestSellerRank;
                        record.KeyFearture = savenotification.KeyFearture;
                        record.ByFeature = savenotification.ByFeature;
                        record.CustomerGroupsInterest = savenotification.CustomerGroupsInterest;
                        record.TechnicalDetail = savenotification.TechnicalDetail;
                        record.UpdatedOn = DateTime.Now;
                        context.SaveChanges();
                    }
                    else
                    {
                        ASIN _asin = new ASIN();
                        _asin.ASIN1 = p.ASIN;
                        _asin.IsScheduled = true;
                        _asin.ScheduleOn = DateTime.Now;
                        _asin.Status = "Executed";
                        record.StartedOn = DateTime.Now;
                        record.EndOn = DateTime.Now;
                        _asin.customer = savenotification.customer;
                        
                        _asin.product = savenotification.product;
                        _asin.CustomerReview = savenotification.CustomerReview;
                        _asin.questionAns = savenotification.questionAns;
                        
                        _asin.bestSellerRank = savenotification.bestSellerRank;
                        _asin.KeyFearture = savenotification.KeyFearture;
                        _asin.ByFeature = savenotification.ByFeature;
                        _asin.CustomerGroupsInterest = savenotification.CustomerGroupsInterest;
                        _asin.TechnicalDetail = savenotification.TechnicalDetail;
                        _asin.CreatedOn = DateTime.Now;
                        context.ASINs.Add(_asin);
                        context.SaveChanges();
                    }
                }
                catch (DbEntityValidationException e)
                {
                    ExceptionHandling.CreateException(e, "Question Answer: " + "saveScrapedDetail", new Dictionary<string, object>() { { p.ASIN, savenotification } });
                    return savenotification;
                }
            }
            return savenotification;
        }

        #region SAVE CUSTOMER LIST
        public bool saveCustomer(List<Customer> lstCust)
        {


            using (AmazonDB context = new AmazonDB())
            {
                var All_listAmazonUserId = lstCust.Select(x => x.AmazonUserId).ToList();

                var existingCustlist = context.Customers.Where(x => All_listAmazonUserId.Contains(x.AmazonUserId)).ToList();
                var toUpdate = existingCustlist.Select(x => x.AmazonUserId).ToList();
                try
                {
                    foreach (var cust in lstCust)
                    {
                        if (toUpdate.Contains(cust.AmazonUserId))
                        {
                            var record = context.Customers.Where(x => x.AmazonUserId == cust.AmazonUserId).FirstOrDefault();
                            bool isUpdate = false;
                            // record.AmazonUserId = cust.AmazonUserId
                            if (record.CustomerFrontURL != cust.CustomerFrontURL)
                            {
                                record.CustomerFrontURL = cust.CustomerFrontURL;
                                isUpdate = true;

                                CustomerHistory custHistory = new CustomerHistory();
                                custHistory.AmazonUserId = cust.AmazonUserId;
                                custHistory.ColumnName = "CustomerFrontURL";
                                custHistory.ColumnValue = record.CustomerFrontURL;
                                custHistory.AddedOn = DateTime.Now;
                                context.CustomerHistories.Add(custHistory);
                            }
                            if (record.ProfileImageURL != cust.ProfileImageURL)
                            {
                                record.ProfileImageURL = cust.ProfileImageURL;
                                isUpdate = true;

                                CustomerHistory custHistory = new CustomerHistory();
                                custHistory.AmazonUserId = cust.AmazonUserId;
                                custHistory.ColumnName = "ProfileImageURL";
                                custHistory.ColumnValue = record.ProfileImageURL;
                                custHistory.AddedOn = DateTime.Now;
                                context.CustomerHistories.Add(custHistory);
                            }
                            if (record.Name != cust.Name)
                            {
                                record.Name = cust.Name;
                                isUpdate = true;

                                CustomerHistory custHistory = new CustomerHistory();
                                custHistory.AmazonUserId = cust.AmazonUserId;
                                custHistory.ColumnName = "Name";
                                custHistory.ColumnValue = record.Name;
                                custHistory.AddedOn = DateTime.Now;
                                context.CustomerHistories.Add(custHistory);
                            }
                            if (record.Designation != cust.Designation)
                            {
                                record.Designation = cust.Designation;
                                isUpdate = true;

                                CustomerHistory custHistory = new CustomerHistory();
                                custHistory.AmazonUserId = cust.AmazonUserId;
                                custHistory.ColumnName = "Designation";
                                custHistory.ColumnValue = record.Designation;
                                custHistory.AddedOn = DateTime.Now;
                                context.CustomerHistories.Add(custHistory);
                            }
                            if (record.About != cust.About)
                            {
                                record.About = cust.About;
                                isUpdate = true;

                                CustomerHistory custHistory = new CustomerHistory();
                                custHistory.AmazonUserId = cust.AmazonUserId;
                                custHistory.ColumnName = "About";
                                custHistory.ColumnValue = record.About;
                                custHistory.AddedOn = DateTime.Now;
                                context.CustomerHistories.Add(custHistory);
                            }
                            if (record.ReviewRanking != cust.ReviewRanking)
                            {
                                record.ReviewRanking = cust.ReviewRanking;
                                isUpdate = true;

                                CustomerHistory custHistory = new CustomerHistory();
                                custHistory.AmazonUserId = cust.AmazonUserId;
                                custHistory.ColumnName = "ReviewRanking";
                                custHistory.ColumnValue = record.ReviewRanking.ToString();
                                custHistory.AddedOn = DateTime.Now;
                                context.CustomerHistories.Add(custHistory);
                            }
                            if (record.HelpfulVotes != cust.HelpfulVotes)
                            {
                                record.HelpfulVotes = cust.HelpfulVotes;
                                isUpdate = true;

                                CustomerHistory custHistory = new CustomerHistory();
                                custHistory.AmazonUserId = cust.AmazonUserId;
                                custHistory.ColumnName = "HelpfulVotes";
                                custHistory.ColumnValue = record.HelpfulVotes.ToString();
                                custHistory.AddedOn = DateTime.Now;
                                context.CustomerHistories.Add(custHistory);
                            }
                            if (record.Reviews != cust.Reviews)
                            {
                                record.Reviews = cust.Reviews;
                                isUpdate = true;

                                CustomerHistory custHistory = new CustomerHistory();
                                custHistory.AmazonUserId = cust.AmazonUserId;
                                custHistory.ColumnName = "Reviews";
                                custHistory.ColumnValue = record.Reviews.ToString();
                                custHistory.AddedOn = DateTime.Now;
                                context.CustomerHistories.Add(custHistory);
                            }
                            if (record.Hearts != cust.Hearts)
                            {
                                record.Hearts = cust.Hearts;
                                isUpdate = true;

                                CustomerHistory custHistory = new CustomerHistory();
                                custHistory.AmazonUserId = cust.AmazonUserId;
                                custHistory.ColumnName = "Hearts";
                                custHistory.ColumnValue = record.Hearts.ToString();
                                custHistory.AddedOn = DateTime.Now;
                                context.CustomerHistories.Add(custHistory);
                            }
                            if (record.IdeaLists != cust.IdeaLists)
                            {
                                record.IdeaLists = cust.IdeaLists;
                                isUpdate = true;

                                CustomerHistory custHistory = new CustomerHistory();
                                custHistory.AmazonUserId = cust.AmazonUserId;
                                custHistory.ColumnName = "IdeaLists";
                                custHistory.ColumnValue = record.IdeaLists.ToString();
                                custHistory.AddedOn = DateTime.Now;
                                context.CustomerHistories.Add(custHistory);
                            }

                            if (isUpdate)
                            {
                                record = cust;
                                record.UpdatedOn = DateTime.Now;
                            }


                        }
                        else
                        {
                            context.Customers.Add(cust);
                        }

                    }
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    ExceptionHandling.CreateException(e, "Question Answer: " + "saveScrapedDetail", new Dictionary<string, object>() { { "Customer", lstCust } });
                    return false;
                }

            }
            return true;


        }
        #endregion

        #region SAVE SELLER LIST
        public bool saveSeller(List<Seller> lstSeller)
        {
            using (AmazonDB context = new AmazonDB())
            {
                var All_lstSeller = lstSeller.Select(x => x.AmazonSellerId).ToList();
                var existinglstSeller = context.Sellers.Where(x => All_lstSeller.Contains(x.AmazonSellerId)).ToList();
                var toUpdate = existinglstSeller.Select(x => x.AmazonSellerId).ToList();
                try
                {
                    foreach (var slr in lstSeller)
                    {
                        if (toUpdate.Contains(slr.AmazonSellerId))
                        {
                            var record = context.Sellers.Where(x => x.AmazonSellerId == slr.AmazonSellerId).FirstOrDefault();
                            bool isUpdate = false;
                            //if (record.AmazonSellerId != slr.AmazonSellerId) { }
                            if (record.ProfileImageURL != slr.ProfileImageURL)
                            {
                                record.ProfileImageURL = slr.ProfileImageURL;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "ProfileImageURL";
                                slrhistory.ColumnValue = record.AmazonSellerId;
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);

                            }
                            if (record.SellerName != slr.SellerName)
                            {
                                record.SellerName = slr.SellerName;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "SellerName";
                                slrhistory.ColumnValue = record.SellerName;
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.SellerContactDetails != slr.SellerContactDetails)
                            {
                                record.SellerContactDetails = slr.SellerContactDetails;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "SellerContactDetails";
                                slrhistory.ColumnValue = record.SellerContactDetails;
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Description != slr.Description)
                            {
                                record.Description = slr.Description;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Description";
                                slrhistory.ColumnValue = record.Description;
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.SellerFrontURL != slr.SellerFrontURL)
                            {
                                record.SellerFrontURL = slr.SellerFrontURL;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "SellerFrontURL";
                                slrhistory.ColumnValue = record.SellerFrontURL;
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.SellerShopURL != slr.SellerShopURL)
                            {
                                record.SellerShopURL = slr.SellerShopURL;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "SellerShopURL";
                                slrhistory.ColumnValue = record.SellerShopURL;
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Rating != slr.Rating)
                            {
                                record.Rating = slr.Rating;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Rating";
                                slrhistory.ColumnValue = record.Rating;
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Positive_30days != slr.Positive_30days)
                            {
                                record.Positive_30days = slr.Positive_30days;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Positive_30days";
                                slrhistory.ColumnValue = record.Positive_30days.ToString();
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Positive_90days != slr.Positive_90days)
                            {
                                record.Positive_90days = slr.Positive_90days;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Positive_90days";
                                slrhistory.ColumnValue = record.Positive_90days.ToString();
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Positive_12Month != slr.Positive_12Month)
                            {
                                record.Positive_12Month = slr.Positive_12Month;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Positive_12Month";
                                slrhistory.ColumnValue = record.Positive_12Month.ToString();
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Positive_LifeTime != slr.Positive_LifeTime)
                            {
                                record.Positive_LifeTime = slr.Positive_LifeTime;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Positive_LifeTime";
                                slrhistory.ColumnValue = record.Positive_LifeTime.ToString();
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Negative_30days != slr.Negative_30days)
                            {
                                record.Negative_30days = slr.Negative_30days;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Negative_30days";
                                slrhistory.ColumnValue = record.Negative_30days.ToString();
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Negative_90days != slr.Negative_90days)
                            {
                                record.Negative_90days = slr.Negative_90days;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Negative_90days";
                                slrhistory.ColumnValue = record.Negative_90days.ToString();
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Negative_12Month != slr.Negative_12Month)
                            {
                                record.Negative_12Month = slr.Negative_12Month;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Negative_12Month";
                                slrhistory.ColumnValue = record.Negative_12Month.ToString();
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Negative_LifeTime != slr.Negative_LifeTime)
                            {
                                record.Negative_LifeTime = slr.Negative_LifeTime;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Negative_LifeTime";
                                slrhistory.ColumnValue = record.Negative_LifeTime.ToString();
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Neutral_30days != slr.Neutral_30days)
                            {
                                record.Neutral_30days = slr.Neutral_30days;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Neutral_30days";
                                slrhistory.ColumnValue = record.Neutral_30days.ToString();
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Neutral_90days != slr.Neutral_90days)
                            {
                                record.Neutral_90days = slr.Neutral_90days;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Neutral_90days";
                                slrhistory.ColumnValue = record.Neutral_90days.ToString();
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Neutral_12Month != slr.Neutral_12Month)
                            {
                                record.Neutral_12Month = slr.Neutral_12Month;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Neutral_12Month";
                                slrhistory.ColumnValue = record.Neutral_12Month.ToString();
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.Neutral_LifeTime != slr.Neutral_LifeTime)
                            {
                                record.Neutral_LifeTime = slr.Neutral_LifeTime;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "Neutral_LifeTime";
                                slrhistory.ColumnValue = record.Neutral_LifeTime.ToString();
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.WhyChooseUs != slr.WhyChooseUs)
                            {
                                record.WhyChooseUs = slr.WhyChooseUs;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "WhyChooseUs";
                                slrhistory.ColumnValue = record.WhyChooseUs;
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.CustomerServicePhone != slr.CustomerServicePhone)
                            {
                                record.CustomerServicePhone = slr.CustomerServicePhone;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "CustomerServicePhone";
                                slrhistory.ColumnValue = record.CustomerServicePhone;
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.CustomerServiceHours != slr.CustomerServiceHours)
                            {
                                record.CustomerServiceHours = slr.CustomerServiceHours;
                                isUpdate = true;

                                SellerHistory slrhistory = new SellerHistory();
                                slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                slrhistory.ColumnName = "CustomerServiceHours";
                                slrhistory.ColumnValue = record.CustomerServiceHours;
                                slrhistory.AddedOn = DateTime.Now;
                                context.SellerHistories.Add(slrhistory);
                            }
                            if (record.SellerProductCount != slr.SellerProductCount)
                            {
                                if (record.SellerProductCount != 0)
                                {
                                    record.SellerProductCount = slr.SellerProductCount;
                                    isUpdate = true;

                                    SellerHistory slrhistory = new SellerHistory();
                                    slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                    slrhistory.ColumnName = "SellerProductCount";
                                    slrhistory.ColumnValue = record.SellerProductCount.ToString();
                                    slrhistory.AddedOn = DateTime.Now;
                                    context.SellerHistories.Add(slrhistory);
                                }
                            }
                            if (record.MarketPlaceID != slr.MarketPlaceID)
                            {
                                if (!string.IsNullOrEmpty(record.MarketPlaceID ))
                                {
                                    record.MarketPlaceID = slr.MarketPlaceID;
                                    isUpdate = true;

                                    SellerHistory slrhistory = new SellerHistory();
                                    slrhistory.AmazonSellerId = slr.AmazonSellerId;
                                    slrhistory.ColumnName = "MarketPlaceID";
                                    slrhistory.ColumnValue = record.MarketPlaceID;
                                    slrhistory.AddedOn = DateTime.Now;
                                    context.SellerHistories.Add(slrhistory);
                                }
                            }
                            if (isUpdate)
                            {
                                record.UpdatedOn = DateTime.Now;
                            }

                        }
                        else
                        {
                            slr.AddedOn = DateTime.Now;
                            context.Sellers.Add(slr);
                        }
                    }
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    ExceptionHandling.CreateException(e, "Question Answer: " + "saveScrapedDetail", new Dictionary<string, object>() { { "Customer", lstSeller } });
                    return false;
                }
            }
            return true;
        }
        #endregion
        public bool saveCustomerReview(List<CustomerReview> lstReview)
        {
            using (AmazonDB context = new AmazonDB())
            {
                var All_lstReview = lstReview.Select(x => x.AmazonReviewId).ToList();
                var existinglstReview = context.CustomerReviews.Where(x => All_lstReview.Contains(x.AmazonReviewId)).ToList();
                var toUpdate = existinglstReview.Select(x => x.AmazonReviewId).ToList();
                try
                {
                    foreach (var review in lstReview)
                    {
                        if (toUpdate.Contains(review.AmazonReviewId))
                        {
                            #region FOR FUTURE
                            //var record = context.CustomerReviews.Where(x => x.AmazonReviewId == review.AmazonReviewId).FirstOrDefault();
                            //bool isUpdate = false;
                            //if (record.ASIN == review.ASIN) {
                            //    ReviewCommentHistory reviewHiostory = new ReviewCommentHistory();
                            //    reviewHiostory.AmazonReviewId = record.AmazonReviewId;
                            //    reviewHiostory.col = record.ReviewTitle;
                            //    reviewHiostory.ReviewDate = record.ReviewDate;
                            //    reviewHiostory.ReviewRating = record.ReviewRating;
                            //    reviewHiostory.Description = record.Description;
                            //    reviewHiostory.Color = record.Color;
                            //    reviewHiostory.Size = record.Size;
                            //    reviewHiostory.PeopleHelpfulCount = record.PeopleHelpfulCount;
                            //    reviewHiostory.IsVerifiedPurchase = record.IsVerifiedPurchase;
                            //    reviewHiostory.IsPositiveReview = record.IsPositiveReview;
                            //    reviewHiostory.IsEarlyReviewerRewards = record.IsEarlyReviewerRewards;
                            //    reviewHiostory.ReviewedBy = record.ReviewedBy;


                            //}
                            //if (record.ReviewTitle == review.ReviewTitle) { }
                            //if (record.ReviewDate == review.ReviewDate) { }
                            //if (record.ReviewRating == review.ReviewRating) { }
                            //if (record.Description == review.Description) { }
                            //if (record.Color == review.Color) { }
                            //if (record.Size == review.Size) { }
                            //if (record.PeopleHelpfulCount == review.PeopleHelpfulCount) { }
                            //if (record.IsVerifiedPurchase == review.IsVerifiedPurchase) { }
                            //if (record.IsPositiveReview == review.IsPositiveReview) { }
                            //if (record.IsEarlyReviewerRewards == review.IsEarlyReviewerRewards) { }
                            //if (record.ReviewedBy == review.ReviewedBy) { }
                            //if (record.DeletedOn == review.DeletedOn) { }
                            //if (record.UpdatedOn == review.UpdatedOn) { }

                            //if (record.CustomerFrontURL != cust.CustomerFrontURL)
                            //{
                            //    record.CustomerFrontURL = cust.CustomerFrontURL;
                            //    isUpdate = true;

                            //    CustomerHistory custHistory = new CustomerHistory();
                            //    custHistory.AmazonUserId = cust.AmazonUserId;
                            //    custHistory.ColumnName = "CustomerFrontURL";
                            //    custHistory.ColumnValue = record.CustomerFrontURL;
                            //    custHistory.AddedOn = DateTime.Now;
                            //    context.CustomerHistories.Add(custHistory);
                            //}
                            //if (isUpdate)
                            //{
                            //    record.UpdatedOn = DateTime.Now;
                            //}
                            #endregion
                        }
                        else
                        {
                            context.CustomerReviews.Add(review);
                        }
                    }
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    ExceptionHandling.CreateException(e, "Customer Review: " + "saveCustomerReview", new Dictionary<string, object>() { { "Review", lstReview } });
                    return false;
                }
                return true;
            }
        }

        public bool saveQnA(List<ProductQuestion> lstProductQuestion)
        {
            using (AmazonDB context = new AmazonDB())
            {
                var All_lstQuestion = lstProductQuestion.Select(x => x.QuestionId).ToList();
                var existinglstQuestion = context.ProductQuestions.Where(x => All_lstQuestion.Contains(x.QuestionId)).ToList();
                var toUpdate = existinglstQuestion.Select(x => x.QuestionId).ToList();
                try
                {
                    foreach (var qst in lstProductQuestion)
                    {
                        if (toUpdate.Contains(qst.QuestionId))
                        {
                            var record = context.ProductQuestions.Where(x => x.QuestionId == qst.QuestionId).FirstOrDefault();
                            //ProductQuestion qns = new ProductQuestion();
                            record = qst;
                            // context.QAMappings.AddRange(qns.QAMappings);
                            //context.ProductQuestions.Add(qns);
                        }
                        else
                        {
                            //context.QAMappings.AddRange(qst.QAMappings);
                            qst.AddedOn = DateTime.Now;
                            context.ProductQuestions.Add(qst);
                        }
                    }
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    ExceptionHandling.CreateException(e, "Product Question: " + "saveQnA", new Dictionary<string, object>() { { "ProductQuestion", lstProductQuestion } });
                    return false;
                }
                return true;
            }
        }

        public bool saveProductSellerMapping(List<ProductSellerMapping> ProductSellerMapping, string ASIN)
        {
            using (AmazonDB context = new AmazonDB())
            {
                var existingSellerMappinglst = context.ProductSellerMappings.Where(x => x.ASIN == ASIN).ToList();
                try
                {
                    foreach (var mapng in ProductSellerMapping)
                    {
                        if (existingSellerMappinglst.Select(x => x.AmazonSellerId).Contains(mapng.AmazonSellerId))
                        {
                            var record = context.ProductSellerMappings.Where(x => x.AmazonSellerId == mapng.AmazonSellerId && x.ASIN == mapng.ASIN).FirstOrDefault();

                            record.Condition = mapng.Condition;
                            record.Price = mapng.Price;
                            record.IsPrime = mapng.IsPrime;
                            record.IsFulfillmentbyAmazon = mapng.IsFulfillmentbyAmazon;
                            record.AddedOn = DateTime.Now;
                            record.IsAtoZGaurantee = mapng.IsAtoZGaurantee;

                        }
                        else
                        {
                            mapng.AddedOn = DateTime.Now;
                            context.ProductSellerMappings.Add(mapng);
                        }
                    }
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    ExceptionHandling.CreateException(e, "Product SellerMapping Answer: " + "saveProductSellerMapping", new Dictionary<string, object>() { { ASIN, ProductSellerMapping } });
                    return false;
                }
                return true;
            }
        }

        public bool saveBSRs(List<ProductBSR> lstProductBSR)
        {
            using (AmazonDB context = new AmazonDB())
            {
                try
                {
                    foreach (var bsr in lstProductBSR)
                    {
                        var record = context.ProductBSRs.Where(x => x.ASIN == bsr.ASIN && x.BestSellerInCategory == bsr.BestSellerInCategory).FirstOrDefault();
                        if (record != null)
                        {
                            if (record.BestSellerRank != bsr.BestSellerRank)
                            {
                                record.BestSellerRank = bsr.BestSellerRank;

                                ProductBSRHistory bsrHistory = new ProductBSRHistory();
                                bsrHistory.ASIN = record.ASIN;
                                bsrHistory.ColumnName = "BestSellerRank";
                                bsrHistory.ColumnValue = record.BestSellerRank.ToString();
                                bsrHistory.AddedOn = DateTime.Now;
                                context.ProductBSRHistories.Add(bsrHistory);
                            }

                            if (record.BestSellerInCategory != bsr.BestSellerInCategory)
                            {
                                record.BestSellerInCategory = bsr.BestSellerInCategory;

                                ProductBSRHistory bsrHistory = new ProductBSRHistory();
                                bsrHistory.ASIN = record.ASIN;
                                bsrHistory.ColumnName = "BestSellerInCategory";
                                bsrHistory.ColumnValue = record.BestSellerInCategory.ToString();
                                bsrHistory.AddedOn = DateTime.Now;
                                context.ProductBSRHistories.Add(bsrHistory);
                            }

                            record.UpdatedOn = DateTime.Now;

                        }
                        else
                        {
                            bsr.CreatedOn = DateTime.Now;
                            context.ProductBSRs.Add(bsr);
                        }
                    }
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    ExceptionHandling.CreateException(e, "BSR: " + "saveBSRs", new Dictionary<string, object>() { { "lstProductBSR", lstProductBSR } });
                    return false;
                }
                return true;
            }
        }

        public bool saveKeyFearture(List<ProductKeyFeature> lstProductKeyFeatures)
        {
            using (AmazonDB context = new AmazonDB())
            {
                try
                {
                    foreach (var keyFeature in lstProductKeyFeatures)
                    {
                        var record = context.ProductKeyFeatures.Where(x => x.ASIN == keyFeature.ASIN && x.KeyFetaure == keyFeature.KeyFetaure).FirstOrDefault();
                        if (record != null)
                        {
                            record.UpdatedOn = DateTime.Now;
                            record.KeyFetaure = keyFeature.KeyFetaure;
                            record.IsHidden = keyFeature.IsHidden;
                        }
                        else
                        {
                            keyFeature.CreatedOn = DateTime.Now;
                            context.ProductKeyFeatures.Add(keyFeature);
                        }
                    }
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    ExceptionHandling.CreateException(e, "KeyFearture: " + "saveKeyFearture", new Dictionary<string, object>() { { "ProductKeyFeatures", lstProductKeyFeatures } });
                    return false;
                }
                return true;
            }
        }

        public bool saveByFeature(List<ByFeature> lstByFeature)
        {
            using (AmazonDB context = new AmazonDB())
            {
                try
                {
                    foreach (var ftr in lstByFeature)
                    {
                        var record = context.ByFeatures.Where(x => x.ASIN == ftr.ASIN && x.Attribute == ftr.Attribute).FirstOrDefault();
                        if (record != null)
                        {

                            if (record.Value != ftr.Value)
                            {
                                record.Value = ftr.Value;

                                ByFeatureHistory ftrHistory = new ByFeatureHistory();
                                ftrHistory.ASIN = record.ASIN;
                                ftrHistory.Attribute = ftr.Attribute;
                                ftrHistory.Value = ftr.Value;
                                ftrHistory.AddedOn = DateTime.Now;
                                context.ByFeatureHistories.Add(ftrHistory);
                            }

                            record.UpdatedOn = DateTime.Now;

                        }
                        else
                        {
                            ftr.AddedOn = DateTime.Now;
                            context.ByFeatures.Add(ftr);
                        }
                    }
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    ExceptionHandling.CreateException(e, "ByFeature: " + "saveByFeature", new Dictionary<string, object>() { { "saveByFeature", lstByFeature } });
                    return false;
                }
                return true;
            }
        }

        public bool saveCustomerGroupsInterest(List<CustomerGroupsInterest> lstCustomerGroupsInterest)
        {
            using (AmazonDB context = new AmazonDB())
            {
                try
                {
                    foreach (var ctrGr in lstCustomerGroupsInterest)
                    {
                        var record = context.CustomerGroupsInterests.Where(x => x.ASIN == ctrGr.ASIN && x.Attribute == ctrGr.Attribute).FirstOrDefault();
                        if (record != null)
                        {

                            if (record.Value != ctrGr.Value)
                            {
                                record.Value = ctrGr.Value;

                                CustomerGroupInterestsHistory crGrHistory = new CustomerGroupInterestsHistory();
                                crGrHistory.ASIN = record.ASIN;
                                crGrHistory.Attribute = ctrGr.Attribute;
                                crGrHistory.Value = ctrGr.Value;
                                crGrHistory.AddedOn = DateTime.Now;
                                context.CustomerGroupInterestsHistories.Add(crGrHistory);
                            }
                            record.UpdatedOn = DateTime.Now;
                        }
                        else
                        {
                            ctrGr.AddedOn = DateTime.Now;
                            context.CustomerGroupsInterests.Add(ctrGr);
                        }
                    }
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    ExceptionHandling.CreateException(e, "CustomerGroupsInterest: " + "saveCustomerGroupsInterest", new Dictionary<string, object>() { { "lstCustomerGroupsInterest", lstCustomerGroupsInterest } });
                    return false;
                }
                return true;
            }
        }

        public bool saveTechnicalDetail(List<TechnicalDetail> lstTechnicalDetail)
        {
            using (AmazonDB context = new AmazonDB())
            {
                try
                {
                    foreach (var tech in lstTechnicalDetail)
                    {
                        var record = context.TechnicalDetails.Where(x => x.ASIN == tech.ASIN && x.Attribute == tech.Attribute).FirstOrDefault();
                        if (record != null)
                        {
                            if (record.Value != tech.Value)
                            {
                                record.Value = tech.Value;
                               
                            }
                            record.UpdatedOn = DateTime.Now;
                        }
                        else
                        {
                            tech.AddedOn = DateTime.Now;
                            context.TechnicalDetails.Add(tech);
                        }
                    }
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    ExceptionHandling.CreateException(e, "TechnicalDetail: " + "saveTechnicalDetail", new Dictionary<string, object>() { { "lstTechnicalDetail", lstTechnicalDetail } });
                    return false;
                }
                return true;
            }
        }
    }
}
