﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazoneASINScrapper.BAL.Models
{
    public class notification
    {
        public bool? customer { get; set; }
        public bool? seller { get; set; }
        public bool? product { get; set; }
        public bool? CustomerReview { get; set; }
        public bool? questionAns { get; set; }
        public bool? ProductSellerMapping { get; set; }
        public bool? bestSellerRank { get; set; }
        public bool? KeyFearture { get; set; }
        public bool? ByFeature { get; set; }
        public bool? CustomerGroupsInterest { get; set; }
        public bool? TechnicalDetail { get; set; }

    }
    public enum enummessage
    {

    }
}
