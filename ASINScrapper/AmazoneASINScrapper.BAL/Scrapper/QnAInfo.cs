﻿using AmazoneASINScrapper.DAL.DBEntity;
using AmazoneASINScrapper.helper;
using ScrapingHelp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazoneASINScrapper.BAL.Scrapper
{
    public class QnAInfo
    {
        #region PRODUCT QUESTION AND ANSWER

        public List<ProductQuestion> getFAQ(string domainUrl, string ASIN)
        {
            int faqPageCout = 1;
            int pageCount = 2;
            List<ProductQuestion> lstProductQuestion = new List<ProductQuestion>();
            while (faqPageCout <= pageCount)
            {
                try
                {
                    string param = "ask/questions/asin/" + ASIN + "/";

                    int retry = 0;
                    FAQSource_retry:
                    var FAQSource = commonHelper.GetSource(domainUrl, param + Convert.ToString(faqPageCout), "");

                    #region RETRY if Source is Empty
                    if (FAQSource == "" && retry < Convert.ToInt32(ConfigurationSettings.AppSettings["_retry"]))
                    {
                        retry++;

                        goto FAQSource_retry;
                    }
                    else if (FAQSource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
                    {
                        retry++;

                        goto FAQSource_retry;
                    }
                    #endregion

                    string[] stringArray = HtmlHelper.CollectUrl(FAQSource, "", "class=\"a-fixed-left-grid a-spacing-base\"", "</div></div></div></div></div>");
                    for (int j = 0; j < stringArray.Length; j++)
                    {
                        ProductQuestion productquestion = new ProductQuestion();

                        // cutomerreview.Id
                        productquestion.ASIN = ASIN;
                        productquestion.Question = HtmlHelper.ReturnValue(stringArray[j], "data-ask-no-op=\"{&quot;metricName&quot;:&quot;top-question-text-click&quot;}\"", ">", "<");
                        productquestion.QuestionId = HtmlHelper.ReturnValue(stringArray[j], "class=\"up\"", "action=\"/ask/vote/question/", "\">");
                        productquestion.NumberOfVote = Convert.ToInt32(HtmlHelper.ReturnValue(stringArray[j], "class=\"count\"", ">", "<").Replace(",", ""));

                        string allAnsLink = HtmlHelper.ReturnValue(stringArray[j], "id=\"askSeeAllAnswersLink", "href=\"", "\">");

                        List<QAMapping> lstQAMapping = new List<QAMapping>();

                        if (allAnsLink.Trim() != "")
                        {
                            int faqPageCoutAns = 1;
                            int pageCountAns = 2;
                            allAnsLink = allAnsLink.Replace("ref", "{0}/ref");
                            while (faqPageCoutAns <= pageCountAns)
                            {
                                int retryAns = 0;
                                retryAns:
                                var FAQAnsSource = commonHelper.GetSource(domainUrl, string.Format(allAnsLink, faqPageCoutAns), "");
                                #region RETRY if Source is Empty
                                if (FAQAnsSource == "" && retryAns < 5)
                                {
                                    retryAns++;

                                    goto retryAns;
                                }
                                else if (FAQAnsSource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
                                {
                                    retryAns++;

                                    goto retryAns;
                                }
                                #endregion

                                if (faqPageCoutAns == 1)
                                {
                                    //string urlFaq = HtmlHelper.ReturnValue(FAQSource, "<li class=\"a-disabled\">...</li>", "href=\"", "<");
                                    string pageSize = HtmlHelper.ReturnValue(FAQAnsSource, "class=\"a-spacing-large a-color-secondary\"", "of", "answers");
                                    if (pageSize.Trim() != "")
                                    {
                                        pageCountAns = Convert.ToInt32(Math.Round(0.5 + (Convert.ToDouble(pageSize) / 10)));
                                    }
                                    //pageCount = Convert.ToInt32(urlFaq.Substring(urlFaq.LastIndexOf('>') + 1));
                                }

                                string[] FAQansArray = FAQAnsSource.Split(new string[] { "id=\"answer-" }, StringSplitOptions.None); //HtmlHelper.CollectUrl(FAQAnsSource, "", "id=\"answer-", "id=\"askPaginationBar\"");

                                for (int k = 1; k < FAQansArray.Length; k++)
                                {
                                    QAMapping qamapping = new QAMapping();

                                    qamapping.Answer = HtmlHelper.ReturnValue(FAQansArray[k], "class=\"a-section\"", "<span>", "</span>");
                                    qamapping.AnsweredBy = HtmlHelper.ReturnValue(FAQansArray[k], "href=\"/gp/profile", "/", "/ref=");

                                    lstQAMapping.Add(qamapping);
                                }
                                faqPageCoutAns++;
                            }
                        }
                        else
                        {
                            QAMapping qamapping = new QAMapping();

                            qamapping.Answer = HtmlHelper.ReturnValue(stringArray[j], "class=\"a-fixed-left-grid-col a-col-right\"", "<span>", "</span>");
                            qamapping.AnsweredBy = HtmlHelper.ReturnValue(stringArray[j], "href=\"/gp/profile", "/", "/ref=");

                            lstQAMapping.Add(qamapping);
                        }

                        productquestion.QAMappings = lstQAMapping;
                        lstProductQuestion.Add(productquestion);
                    }




                    if (faqPageCout == 1)
                    {
                        //string urlFaq = HtmlHelper.ReturnValue(FAQSource, "<li class=\"a-disabled\">...</li>", "href=\"", "<");
                        string tottalPage = HtmlHelper.ReturnValue(FAQSource, "class=\"a-section askPaginationHeaderMessage\"", "of", "questions").Replace("+", "");
                        if (tottalPage != "")
                        {
                            pageCount = Convert.ToInt32(Math.Round(0.5 + (Convert.ToDouble(tottalPage) / 10)));
                        }
                        //pageCount = Convert.ToInt32(urlFaq.Substring(urlFaq.LastIndexOf('>') + 1));
                    }
                    faqPageCout++;
                }
                catch (Exception ex)
                {
                    ExceptionHandling.CreateException(ex, "Question Answer: " + ASIN.ToString(), null);
                }
            }

            //using (DBModels context = new DBModels())
            //{
            //    context.BulkInsert(lstProductQuestion, options =>
            //    {
            //        options.InsertIfNotExists = true;
            //        options.AllowUpdatePrimaryKeys = true;
            //    });
            //}

            //p.ProductQuestions = lstProductQuestion;
            return lstProductQuestion;
        }
        #endregion
    }
}
