﻿using HtmlAgilityPack;
using ScrapingHelp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AmazoneASINScrapper.DAL.DBEntity;
using AmazoneASINScrapper.helper;
using System.Net;
using System.IO;

namespace AmazoneASINScrapper.BAL.Scrapper
{
    public class productInfo
    {
        #region BASIC PRODUCT INFORMATION

        public ProductDetail getProductDetail(string domainUrl, string ASIN)
        {
            ProductDetail p = new ProductDetail();
            try
            {
                int retry = 0;
            retry_mainsource:
                var mainsource = commonHelper.GetSource(domainUrl, "dp/" + ASIN, "");
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(mainsource);
                #region RETRY if Source is Empty
                if (mainsource == "" && retry < Convert.ToInt32(ConfigurationSettings.AppSettings["_retry"]))
                {
                    retry++;

                    goto retry_mainsource;
                }
                else if (mainsource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
                {
                    retry++;

                    goto retry_mainsource;
                }
                #endregion
                #region check For SellerID

                string AmazonSellersId = string.Empty;
                var sellerDiv = doc.GetElementbyId("merchant-info");
                if (sellerDiv == null)
                {
                    int retry2 = 0;
                sellerProfileSource_retry2:
                    mainsource = commonHelper.GetSource(domainUrl + "dp/" + ASIN, "?psc=1", "");
                    #region RETRY if Source is Empty
                    if (mainsource == "" && retry < Convert.ToInt32(ConfigurationSettings.AppSettings["_retry"]))
                    {
                        retry2++;

                        goto sellerProfileSource_retry2;

                    }
                    else if (mainsource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
                    {
                        retry2++;

                        goto sellerProfileSource_retry2;
                    }
                    #endregion
                    doc = new HtmlDocument();
                    doc.LoadHtml(mainsource);

                }

                #endregion
                p = productDetail(domainUrl, mainsource, doc, ASIN);
                p.TechnicalDetails = productTechnicalDetail(mainsource, ASIN, doc);
            }
            catch (Exception ex)
            {
                ExceptionHandling.CreateException(ex, ASIN.ToString(), null);
            }

            return p;
        }

        public ProductDetail productDetail(string domainUrl, string mainsource, HtmlDocument doc, string ASIN)
        {
            ProductDetail p = new ProductDetail();
            string[] domainName = domainUrl.Split('.');
            p.DomainName = domainName[2].ToString().TrimEnd('/');

            var qid = doc.GetElementbyId("qid");
            if (qid != null)
                //p.Qid = HtmlHelper.ReturnValue(qid.OuterHtml, "<input type=\"hidden\" id=\"qid\" name=\"qid\" value=\"", "\">");
                p.Qid = qid.InnerHtml;

            p.ASIN = ASIN;

            p.Title = HtmlHelper.ReturnValue(mainsource, "id=\"productTitle\" class=\"a-size-large\">", "</");
            if (p.Title == "")
            {
                p.Title = HtmlHelper.ReturnValue(mainsource, "<div id=\"item_name\">", "</");
            }
            if (p.Title == "")
            {
                p.Title = HtmlHelper.ReturnValue(mainsource, "<span id=\"productTitle\"", ">", "<");
            }
            var Description_elm = doc.GetElementbyId("productDescription");
            if (Description_elm != null)
                p.Description = Regex.Replace(Description_elm.InnerHtml, "<.*?>", string.Empty);

            var elm = doc.GetElementbyId("altImages");

            if (elm != null)
            {
                string short_src = elm.InnerHtml;
                string[] imageArray = short_src.Split(new string[] { "<li" }, StringSplitOptions.None);
                if (imageArray.Length > 2)
                {
                    p.ImageLink = HtmlHelper.ReturnValue(imageArray[1], "<img", "src=\"", "\"");
                    if (imageArray.Length > 3)
                        p.ImageLink1 = HtmlHelper.ReturnValue(imageArray[2], "<img", "src=\"", "\"");
                    if (imageArray.Length > 4)
                        p.ImageLink2 = HtmlHelper.ReturnValue(imageArray[3], "<img", "src=\"", "\"");
                    if (imageArray.Length > 5)
                        p.ImageLink3 = HtmlHelper.ReturnValue(imageArray[4], "<img", "src=\"", "\"");
                    if (imageArray.Length > 6)
                        p.ImageLink4 = HtmlHelper.ReturnValue(imageArray[5], "<img", "src=\"", "\"");
                    if (imageArray.Length > 7)
                        p.ImageLink5 = HtmlHelper.ReturnValue(imageArray[6], "<img", "src=\"", "\"");
                    if (imageArray.Length > 8)
                        p.ImageLink6 = HtmlHelper.ReturnValue(imageArray[7], "<img", "src=\"", "\"");

                    p.VideoLink = HtmlHelper.ReturnValue(short_src, "class=\"a-spacing-small item videoBlockIngress videoBlockDarkIngress\"", "src=\"", "\"");
                }
            }

            var ourprc = doc.GetElementbyId("priceblock_ourprice");
            if (ourprc != null)
            {
                p.Price = Regex.Replace(ourprc.InnerHtml.Trim().Replace("$", ""), "<.*?>", string.Empty);
            }

            p.Price = HtmlHelper.formatString(HtmlHelper.ReturnValue(mainsource, "id=\"priceblock_ourprice\"", "\">", "</span>")).Trim().Replace("$", "");
            p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "<span class=\"a-size-large a-color-price\">", "</").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "id=\"priceblock_ourprice\"", ">", "</").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "id=\"priceblock_dealprice\"", ">", "</").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "id=\"priceblock_dealprice\"", ">", "</").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "class=\"a-size-large a-color-price\"", ">", "<").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "\"sims-fbt-this-item a-text-bold\"", "'p13n-sc-price'>", "<").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.ReturnValue(mainsource, "Price:", "class=\"a-size-medium a-color-price\">", "<").Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }

            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.formatString(HtmlHelper.ReturnValue(mainsource, "\"verticalAlign a-size-large\">", "</div>")).Trim().Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
                if (!p.Price.Contains(".") && p.Price.Length > 2)
                {
                    string s = p.Price.Substring(p.Price.Length - 2);
                    string m = p.Price.Substring(0, p.Price.Length - 2).Replace("  ", "").Replace(" ", "");
                    string t = "." + s;
                    p.Price = m + t;
                }
            }
            if (p.Price == string.Empty)
            {
                p.Price = HtmlHelper.formatString(HtmlHelper.ReturnValue(mainsource, "from&nbsp;", "</div>")).Replace("$", "");
                p.Price = Regex.Replace(p.Price, "<.*?>", string.Empty);
            }
            if (p.Price == string.Empty)
            {
                string sortprice = HtmlHelper.ReturnValue(mainsource, "priceInfoData=", "/span>");
                p.Price = HtmlHelper.ReturnValue(sortprice, "\">", "<");
            }

            p.ListPrice = HtmlHelper.ReturnValue(mainsource, "class=\"a-size-small a-text-strike\">", "</span>").Trim().Replace("$", "");
            p.ListPrice = Regex.Replace(p.ListPrice, "<.*?>", string.Empty);
            if (p.ListPrice == string.Empty)
            {
                p.ListPrice = HtmlHelper.ReturnValue(mainsource, "List Price:", "class=\"a-text-strike\">", "<").Trim().Replace("$", "");
                p.ListPrice = Regex.Replace(p.ListPrice, "<.*?>", string.Empty);
            }
            if (p.ListPrice == string.Empty)
            {
                p.ListPrice = HtmlHelper.ReturnValue(mainsource, "List Price:", "class=\"a-text-strike\">", "<").Trim().Replace("$", "");
                p.ListPrice = Regex.Replace(p.ListPrice, "<.*?>", string.Empty);
            }
            if (p.ListPrice == string.Empty)
            {
                p.ListPrice = HtmlHelper.ReturnValue(mainsource, "Was Price:", "class=\"a-text-strike\">", "<").Trim().Replace("$", "");
                p.ListPrice = Regex.Replace(p.ListPrice, "<.*?>", string.Empty);
            }
            if (p.ListPrice == string.Empty)
            {
                p.ListPrice = HtmlHelper.ReturnValue(mainsource, "class=\"a-size-small a-text-strike\"", ">", "<").Trim().Replace("$", "");
                p.ListPrice = Regex.Replace(p.ListPrice, "<.*?>", string.Empty);
            }
            if (p.ListPrice == string.Empty || p.ListPrice == null)
            {
                p.ListPrice = HtmlHelper.ReturnValue(mainsource, "class=\"a-span12 a-color-secondary a-size-base\"", "class=\"a-text-strike\">", "<").Trim().Replace("$", "");
                p.ListPrice = Regex.Replace(p.ListPrice, "<.*?>", string.Empty);
            }
            if (p.ListPrice == string.Empty || p.ListPrice == null)
            {
                p.ListPrice = HtmlHelper.ReturnValue(mainsource, "class=\"priceBlockStrikePriceString a-text-strike\"", ">", "<").Trim().Replace("$", "");
                p.ListPrice = Regex.Replace(p.ListPrice, "<.*?>", string.Empty);
            }




            var ele_YouSave = doc.GetElementbyId("regularprice_savings");
            if (ele_YouSave != null)
            {
                p.YouSave = HtmlHelper.ReturnValue(ele_YouSave.InnerHtml, "class=\"a-span12 a-color-price a-size-base priceBlockSavingsString\"", ">", "<").Trim().Replace("$", "");
                p.YouSave = Regex.Replace(p.YouSave, "<.*?>", string.Empty);
            }
            else
            {
                p.YouSave = "";
            }


            string[] T_c = HtmlHelper.formatString(HtmlHelper.ReturnValue(mainsource, "<title>", "</title>")).Split(':');

            string productNam = HtmlHelper.ReturnValue(mainsource, "id=\"productTitle", ">", "<");
            if (T_c.Length > 1)
                p.ProductCategory = T_c[T_c.Length - 1];

            var productSiteMap = doc.GetElementbyId("wayfinding-breadcrumbs_feature_div");
            if (productSiteMap != null)
            {
                string[] siteArray = HtmlHelper.CollectUrl(productSiteMap.InnerHtml, "", "<li", "</li>");
                p.ProductCategory = HtmlHelper.ReturnValue(siteArray[siteArray.Length - 1], "class=\"a-link-normal a-color-tertiary\"", ">", "<");
            }


            string ShortContent = HtmlHelper.ReturnValue(mainsource, "<div class=\"a-column a-span6\">", "</table>");

            string Review = HtmlHelper.ReturnValue(ShortContent, "id=\"acrCustomerReviewText\"", ">", "</");
            if (Review == string.Empty)
            {
                Review = HtmlHelper.ReturnValueBefore(ShortContent, "customer reviews", ">");
            }
            if (Review == string.Empty)
            {
                Review = HtmlHelper.ReturnValue(mainsource, "id=\"acrCustomerReviewText", ">", "<");
            }
            Review = Review.Replace("customer reviews", "");
            string Rating = HtmlHelper.ReturnValue(ShortContent, "class=\"a-icon-alt\">", "</").Replace("<span>", "");
            if (Rating.ToLower().Contains("back") || Rating.ToLower().Contains("previous page") || Rating.ToLower().Contains("next page") || Rating.ToLower().Contains("prime") || Rating.ToLower().Contains("next") || Rating.ToLower().Contains("facebook") || Rating.ToLower().Contains("twitter"))
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "<span id=\"acrPopover\"", "title=\"", "\">");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "id=\"reviewStarsLinkedCustomerReviews\"", "alt\">", "</").Replace("<span>", "");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(ShortContent, "out of 5 stars\">", "</").Replace("<span>", "");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "-rating-out-of-text\">", ">", "<");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "<span id=\"acrPopover\"", "title=\"", "\">");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "average_customer_reviews", "span class=\"a-icon-alt\">", "</span>");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "a-icon a-icon-star a-star-4-5", "a-icon-alt\">", "</span>");
            }
            if (Rating == string.Empty)
            {
                Rating = HtmlHelper.ReturnValue(mainsource, "\"arp-rating-out-of-text\">", "</span>");
            }
            if (Rating.Contains("Facebook") || Rating.Contains("Twitter") || Rating.Contains("Pinterest") || Rating.Contains("Back") || Rating.Contains("Prime") || Rating.Contains("Next") || Rating.Contains("Previous page") || Rating.Contains("Next page"))
            {
                Rating = "";
            }

            p.CustomerRating = Rating.Trim() != "" ? Convert.ToDecimal(Rating.Replace(" out of 5 stars", "")) : 0;
            p.CustomerRatingCount = Review.Trim() != "" ? Convert.ToInt32(Review.Replace(",", "").Replace(" ratings", "")) : 0;
            //p.IsPrime = "";

            string AmazonSellerId = string.Empty;
            var sellerDiv = doc.GetElementbyId("merchant-info");
            if (sellerDiv.InnerHtml.Trim().Contains("Ships from and sold by Amazon.com.".Trim()))
            {
                AmazonSellerId = "Amazon.com";
            }
            else
            {
                string sellerInnerstring = sellerDiv.InnerHtml;
                AmazonSellerId = HtmlHelper.ReturnValue(sellerInnerstring, "href=\"", "seller=", "&");
                if (AmazonSellerId == "")
                {
                    AmazonSellerId = HtmlHelper.ReturnValue(sellerInnerstring, "seller=", "&");
                }
                if (AmazonSellerId == "")
                {
                    var sllerLink = doc.GetElementbyId("sellerProfileTriggerId");
                    if (sllerLink != null)
                    {
                        string str = sllerLink.Attributes["href"].Value;
                        AmazonSellerId = HtmlHelper.ReturnValue(str, "seller=", "&");
                    }

                }

            }
            if (AmazonSellerId == "")
            {
                AmazonSellerId = HtmlHelper.ReturnValue(mainsource, "mbc-seller-information.html", ";me=", "&amp;");
            }
            
            
            p.AmazonSellerId = AmazonSellerId;

            var ele_allbuyingOption = doc.GetElementbyId("buybox-see-all-buying-choices-announce");
            if (ele_allbuyingOption != null)
            {
                p.AmazonSellerId = "alloption";
            }

            if (p.AmazonSellerId == null || p.AmazonSellerId == "")
            {
                p.AmazonSellerId = "NA";
            }


            string amazon_choice = HtmlHelper.formatString(HtmlHelper.ReturnValue(mainsource, "class=\"ac-badge-text-primary\">", "</a>")).Trim().TrimEnd();
            p.IsAmazonChoice = amazon_choice != "" ? true : false;

            List<ProductBSR> lstbsr = new List<ProductBSR>();
            var bestSellerRankElement = doc.GetElementbyId("SalesRank");
            string bestSellerRankShortString = string.Empty;
            if (bestSellerRankElement != null)
            {
                bestSellerRankShortString = bestSellerRankElement.InnerHtml;

                ProductBSR bsrtop = new ProductBSR();
                bsrtop.ASIN = ASIN;
                bsrtop.BestSellerRank = HtmlHelper.ReturnValue(bestSellerRankShortString, "</b>", "#", " in ").Trim() != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(bestSellerRankShortString, "</b>", "#", " in ").Replace(",", "")) : 0;
                bsrtop.BestSellerInCategory = HtmlHelper.ReturnValue(bestSellerRankShortString, "</b>", " in ", "(<a href=");
                lstbsr.Add(bsrtop);

                string[] stringArray = HtmlHelper.CollectUrl(bestSellerRankShortString, "", "<li", "</li>");
                for (int i = 0; i < stringArray.Length; i++)
                {
                    ProductBSR bsr = new ProductBSR();
                    bsr.ASIN = ASIN;
                    bsr.BestSellerRank = HtmlHelper.ReturnValue(stringArray[i], "class=\"zg_hrsr_rank\">", "#", "</span>").Trim() != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(stringArray[i], "class=\"zg_hrsr_rank\">", "#", "</span>").Replace(",", "")) : 0;
                    bsr.BestSellerInCategory = HtmlHelper.ReturnValue(stringArray[i], "<a href=", ">", "</a>");
                    lstbsr.Add(bsr);
                }

            }
            else
            {
                bestSellerRankShortString = HtmlHelper.ReturnValue(mainsource, "Best Sellers Rank", "<td>", "</td>");
                if (bestSellerRankShortString.Trim() == "")
                {
                    bestSellerRankShortString = HtmlHelper.ReturnValue(mainsource.ToLower(), "Amazon bestseller rank".ToLower(), "<td>", "</td>");
                }
                string[] stringArray = HtmlHelper.CollectUrl(bestSellerRankShortString, "", "<span", "</span>");

                for (int i = 0; i < stringArray.Length; i++)
                {
                    ProductBSR bsr = new ProductBSR();
                    bsr.ASIN = ASIN;
                    bsr.BestSellerRank = HtmlHelper.ReturnValue(stringArray[i], ">", "#", " in").Trim() != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(stringArray[i], ">", "#", " in").Replace(",", "")) : 0;
                    bsr.BestSellerInCategory = HtmlHelper.ReturnValue(stringArray[i], "<a href=", ">", "</a>").Replace("See Top 100 in ", "");
                    lstbsr.Add(bsr);
                }


            }
            p.ProductBSRs = lstbsr;
            p.WarrentyAndSupport = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Warranty & Support", "table-padding\">", "</div>"), "<.*?>", string.Empty);
            string shipWeight = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Shipping Weight", "class=\"a-size-base\">", "("), "<.*?>", string.Empty);
            string[] weightstr = shipWeight.Split(' ');
            if (weightstr.Length > 0)
            {
                decimal Weight = 0;
                if (weightstr[0].Trim() != "")
                    decimal.TryParse(weightstr[0], out Weight);
                p.ShippingWeight = Weight == 0 ? (decimal?)null : Weight;
            }
            var shipDetail = doc.GetElementbyId("olp-upd-new-freeshipping");
            if (shipDetail != null)
            {
                p.ShippingDetails = Regex.Replace(HtmlHelper.ReturnValue(shipDetail.InnerHtml, "class=\"a-color-base\"", ">", "<"), "<.*?>", string.Empty); ;
            }
            var ele_replacementInfo = doc.GetElementbyId("icon-farm-containe");
            if (ele_replacementInfo != null)
            {
                if (ele_replacementInfo.InnerHtml.Trim() == "10 days Replacement".Trim())
                { p.Is10daysReplacement = true; }
                else { p.Is10daysReplacement = false; }

                if (ele_replacementInfo.InnerHtml.Trim() == "30 days Replacement".Trim())
                { p.Is30daysReplacement = true; }
                else { p.Is30daysReplacement = false; }

                if (ele_replacementInfo.InnerHtml.Trim() == "60 days Replacement".Trim())
                { p.Is60daysReplacement = true; }
                else { p.Is60daysReplacement = false; }

            }


            string ShippingInfo = "";
            ShippingInfo = "Domestic Shipping: " + Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Domestic Shipping:", ">", "</"), "<.*?>", string.Empty);
            ShippingInfo = " International Shipping:" + ShippingInfo + Regex.Replace(HtmlHelper.ReturnValue(mainsource, "International Shipping:", ">", "</"), "<.*?>", string.Empty);
            ShippingInfo = " Shipping: " + Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Shipping:", ">", "</"), "<.*?>", string.Empty);

            p.ShippingDetails = ShippingInfo;

            p.ProductDimensions = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Package Dimensions", ">", "</"), "<.*?>", string.Empty);
            if (p.ProductDimensions.Trim() == "")
            {

                p.ProductDimensions = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Product Dimensions", ">", "</"), "<.*?>", string.Empty);
            }
            p.ItemModelNumber = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Item model number", ">", "</"), "<.*?>", string.Empty);
            p.ItemWeight = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Item Weight", "class=\"a-size-base\">", "</td>"), "<.*?>", string.Empty);
            p.DateFirstListedOnAmazon = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Date First Available", ">", "<"), "<.*?>", string.Empty);

            p.DateFirstListedOnAmazon = Regex.Replace(HtmlHelper.ReturnValue(mainsource, "Date First Available", "class=\"a-size-base\">", "</td>"), "<.*?>", string.Empty); ;

            p.IsAplusContent = false;

            var AplusContent = doc.GetElementbyId("aplus");
            if (AplusContent == null)
            {
                var AplusContentCheck2 = doc.GetElementbyId("aplus_feature_div");
                if (AplusContentCheck2 != null)
                {
                    p.IsAplusContent = true;
                }
            }
            else
            {
                p.IsAplusContent = true;
            }

            p.IsInStock = true;
            var ele_availability = doc.GetElementbyId("availability");
            if (ele_availability != null)
            {
                if (ele_availability.InnerHtml.Trim().Contains("Currently unavailable.".Trim()))
                {
                    p.IsInStock = false;
                }
            }
            var ele_productFeatureBullets = doc.GetElementbyId("feature-bullets");
            if (ele_productFeatureBullets != null)
            {
                p.ProductKeyFeatures = productFeatureBullets(ele_productFeatureBullets.InnerHtml, ASIN);
            }


            #region
            List<CustomerGroupsInterest> lstgroupsinterest = new List<CustomerGroupsInterest>();
            List<ByFeature> lstByFeature = productByFeatureDetail(domainUrl, ASIN, out lstgroupsinterest);

            p.ByFeatures = lstByFeature;
            p.CustomerGroupsInterests = lstgroupsinterest;
            #endregion

            return p;
        }

        public List<ProductKeyFeature> productFeatureBullets(string bulletString, string ASIN)
        {
            List<ProductKeyFeature> lstkeyfeature = new List<ProductKeyFeature>();
            string[] FeatureBulletsArray = HtmlHelper.CollectUrl(bulletString, "", "<li", "</li>");
            for (int i = 0; i < FeatureBulletsArray.Length; i++)
            {
                ProductKeyFeature keyfeature = new ProductKeyFeature();
                keyfeature.ASIN = ASIN;
                keyfeature.KeyFetaure = Regex.Replace(HtmlHelper.ReturnValue(FeatureBulletsArray[i], "<span", ">", "</span>"), "<.*?>", string.Empty);
                keyfeature.IsHidden = false;
                if (FeatureBulletsArray[i].Contains("class=\"aok-hidden\""))
                {
                    keyfeature.IsHidden = true;
                }
                lstkeyfeature.Add(keyfeature);
            }
            return lstkeyfeature;
        }

        public List<TechnicalDetail> productTechnicalDetail(string mainsource, string ASIN, HtmlDocument doc)
        {

            string short_src = HtmlHelper.ReturnValue(mainsource, "id=\"productDetails_detailBullets_sections1\"", "</table>");
            string[] stringArray = HtmlHelper.CollectUrl(short_src, "", "<tr>", "</tr>");
            List<TechnicalDetail> lstTechdetail = new List<TechnicalDetail>();
            for (int i = 0; i < stringArray.Length; i++)
            {
                TechnicalDetail techdetail = new TechnicalDetail();
                techdetail.ASIN = ASIN;
                techdetail.Attribute = HtmlHelper.ReturnValue(stringArray[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>");
                if (HtmlHelper.ReturnValue(stringArray[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>").Trim() == "Customer Reviews".Trim())
                {
                    techdetail.Value = HtmlHelper.ReturnValue(stringArray[i], "id=\"acrPopover\"", "title=\"", "\"") + " - " + HtmlHelper.ReturnValue(stringArray[i], "id=\"acrCustomerReviewText\"", ">", "</span>");
                }
                else
                {
                    techdetail.Value = Regex.Replace(HtmlHelper.ReturnValue(stringArray[i], "<td", ">", "</td>"), "<.*?>", string.Empty);
                }
                lstTechdetail.Add(techdetail);
            }

            var ele_techSpec_section_1 = doc.GetElementbyId("productDetails_techSpec_section_1");
            if (ele_techSpec_section_1 != null)
            {
                string techSpec_section_1 = ele_techSpec_section_1.InnerHtml;
                string[] techSpec_section_1Array = HtmlHelper.CollectUrl(techSpec_section_1, "", "<tr>", "</tr>");

                for (int i = 0; i < techSpec_section_1Array.Length; i++)
                {
                    TechnicalDetail techdetail = new TechnicalDetail();
                    techdetail.ASIN = ASIN;
                    techdetail.Attribute = HtmlHelper.ReturnValue(techSpec_section_1Array[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>");
                    if (HtmlHelper.ReturnValue(techSpec_section_1Array[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>").Trim() == "Customer Reviews".Trim())
                    {
                        techdetail.Value = HtmlHelper.ReturnValue(techSpec_section_1Array[i], "id=\"acrPopover\"", "title=\"", "\"") + " - " + HtmlHelper.ReturnValue(stringArray[i], "id=\"acrCustomerReviewText\"", ">", "</span>");
                    }
                    else
                    {
                        techdetail.Value = Regex.Replace(HtmlHelper.ReturnValue(techSpec_section_1Array[i], "<td", ">", "</td>"), "<.*?>", string.Empty);
                    }
                    lstTechdetail.Add(techdetail);
                }
            }
            ///////////////////
            var ele_techSpec_section_2 = doc.GetElementbyId("productDetails_techSpec_section_2");
            if (ele_techSpec_section_2 != null)
            {
                string techSpec_section_2 = ele_techSpec_section_2.InnerHtml;
                string[] techSpec_section_2Array = HtmlHelper.CollectUrl(techSpec_section_2, "", "<tr>", "</tr>");

                for (int i = 0; i < techSpec_section_2Array.Length; i++)
                {
                    TechnicalDetail techdetail = new TechnicalDetail();
                    techdetail.ASIN = ASIN;
                    techdetail.Attribute = HtmlHelper.ReturnValue(techSpec_section_2Array[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>");
                    if (HtmlHelper.ReturnValue(techSpec_section_2Array[i], "class=\"a-color-secondary a-size-base prodDetSectionEntry\"", ">", "</th>").Trim() == "Customer Reviews".Trim())
                    {
                        techdetail.Value = HtmlHelper.ReturnValue(techSpec_section_2Array[i], "id=\"acrPopover\"", "title=\"", "\"") + " - " + HtmlHelper.ReturnValue(stringArray[i], "id=\"acrCustomerReviewText\"", ">", "</span>");
                    }
                    else
                    {
                        techdetail.Value = Regex.Replace(HtmlHelper.ReturnValue(techSpec_section_2Array[i], "<td", ">", "</td>"), "<.*?>", string.Empty);
                    }
                    lstTechdetail.Add(techdetail);
                }
            }
            return lstTechdetail;
        }

        public List<ByFeature> productByFeatureDetail(string domainUrl, string ASIN, out List<CustomerGroupsInterest> lstgroupsinterest)
        {
            string urlparam = "/hz/reviews-render/ajax/lazy-widgets/stream?asin=" + ASIN + "&lazyWidget=cr-summarization-attributes&lazyWidget=cr-skyfall&lazyWidget=cr-solicitation&lazyWidget=cr-summarization-lighthut";
            int retry = 0;
        retry_strContent:
            string strContent = commonHelper.GetSource(domainUrl, urlparam, "");
            #region RETRY if Source is Empty
            if (strContent == "" && retry < Convert.ToInt32(ConfigurationSettings.AppSettings["_retry"]))
            {
                retry++;
                goto retry_strContent;
            }
            else if (strContent.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
            {
                retry++;
                goto retry_strContent;
            }
            #endregion

            List<ByFeature> lstByFeature = new List<ByFeature>();
            string featuresource = HtmlHelper.ReturnValue(HtmlHelper.ReturnFormatedHtml(strContent.Replace("\\", "")), "cr-summarization-attributes\"", "cr-lazy-widget.cr-skyfall"); ;
            string[] Allfeature = HtmlHelper.CollectUrl(featuresource, "", "data-hook=\"cr-summarization-attribute\"", "</span></div></div></div></div>");
            foreach (string values in Allfeature)
            {
                ByFeature byfeature = new ByFeature();
                byfeature.Attribute = HtmlHelper.formatString(HtmlHelper.ReturnValue(values, "class=\"a-section a-spacing-none\">", "</span></div></div>")).Trim();
                string val = HtmlHelper.ReturnValue(values, "class=\"a-icon-alt\">", "</span></i>");
                byfeature.Value = val.Trim() != "" ? Convert.ToDecimal(val) : 0;
                byfeature.ASIN = ASIN;
                lstByFeature.Add(byfeature);
            }

            List<CustomerGroupsInterest> lstcustomergroupsinterest = new List<CustomerGroupsInterest>();
            string _customergrp = HtmlHelper.ReturnValue(strContent.Replace("\\", ""), "cr-lazy-widget.cr-skyfall", "class=\"cr-skyfall-feedback-section\">");
            string[] Allcustomergroups = HtmlHelper.CollectUrl(_customergrp, "", "data-hook=\"cm_cr_skyfall_medley_category\"", "</div></div></div></span>");
            foreach (string _values in Allcustomergroups)
            {
                CustomerGroupsInterest customergroupsinterest = new CustomerGroupsInterest();
                customergroupsinterest.Attribute = HtmlHelper.formatString(HtmlHelper.ReturnValue(_values, "class=\"a-fixed-right-grid a-spacing-base\">", "</span></div></div>")).Trim();
                string val = HtmlHelper.ReturnValue(_values, "class=\"a-size-base a-color-tertiary\">", "</span>");
                customergroupsinterest.Value = val.Trim() != "" ? Convert.ToDecimal(val) : 0;
                customergroupsinterest.ASIN = ASIN;
                lstcustomergroupsinterest.Add(customergroupsinterest);
            }
            lstgroupsinterest = lstcustomergroupsinterest;
            return lstByFeature;
        }

        #endregion
    }
}
