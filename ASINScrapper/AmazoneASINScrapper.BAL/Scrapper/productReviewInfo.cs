﻿using AmazoneASINScrapper.DAL.DBEntity;
using AmazoneASINScrapper.helper;
using ScrapingHelp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AmazoneASINScrapper.BAL.Scrapper
{
    public class productReviewInfo
    {
        #region PRODUCT REVIEW DATA

        public List<CustomerReview> getCutomerReview(string domainUrl, string ASIN, out List<Customer> customerLst, List<CustomerReview> lstOldReviews)
        {
            int reviewPageCout = 1;
            int pageCount = 2;
            List<CustomerReview> lstCutomerReview = new List<CustomerReview>();
            List<Customer> lstCutomer = new List<Customer>();
            while (reviewPageCout <= pageCount)
            {
                try
                {
                    int retry = 0;
                ReviewSource_retry:
                    var ReviewSource = commonHelper.GetSource(domainUrl, "product-reviews/" + ASIN + "?ie=UTF8" + "&pageNumber=" + reviewPageCout + "&pageSize=20", "");

                    #region RETRY if Source is Empty
                    if (ReviewSource == "" && retry < Convert.ToInt32(ConfigurationSettings.AppSettings["_retry"]))
                    {
                        retry++;

                        goto ReviewSource_retry;
                    }
                    else if (ReviewSource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
                    {
                        retry++;

                        goto ReviewSource_retry;
                    }
                    #endregion


                    string count = HtmlHelper.ReturnValue(ReviewSource, "Showing 1-20 of", " ", " reviews");
                    if (count.Trim() != "")
                    {


                        if (reviewPageCout == 1)
                        {
                            pageCount = Convert.ToInt32(Math.Round(0.5 + (Convert.ToDouble(count) / 20)));
                        }
                    }
                    reviewPageCout++;
                    #region

                    string[] stringArray = HtmlHelper.CollectUrl(ReviewSource, "", "data-hook=\"review\"", "</div></div></div></div></div></div>");
                    for (int j = 0; j < stringArray.Length; j++)
                    {
                        CustomerReview cutomerreview = new CustomerReview();
                        cutomerreview.AmazonReviewId = HtmlHelper.ReturnValue(stringArray[j], "<div id=\"customer_review", "-", "\" class=");
                        var newStringArray = lstOldReviews.Where(x => x.AmazonReviewId.Contains(cutomerreview.AmazonReviewId)).Select(x => x.AmazonReviewId);
                        if (newStringArray == null)
                        {
                            // cutomerreview.Id
                            string customerName = Regex.Replace(HtmlHelper.ReturnValue(stringArray[j], "class=\"a-profile-name\"", ">", "<"), "<.*?>", string.Empty);
                            decimal rating = Convert.ToDecimal(HtmlHelper.ReturnValue(stringArray[j], "class=\"a-link-normal\"", "title=\"", " out of 5 stars").Trim() != "" ? HtmlHelper.ReturnValue(stringArray[j], "class=\"a-link-normal\"", "title=\"", " out of 5 stars").Replace(",", "").Trim() : "0.0");
                            cutomerreview.ASIN = ASIN;

                            cutomerreview.ReviewTitle = Regex.Replace(HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"review-title\"", "<span>", "</span>"), "<.*?>", string.Empty);//Regex.Replace(HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"review-title\"", "<span class=\"cr-original-review-content\">", "</span>"), "<.*?>", string.Empty);
                            if (cutomerreview.ReviewTitle.Trim() == "" || cutomerreview.ReviewTitle.Trim() == null)
                            {
                                cutomerreview.ReviewTitle = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"review-title\"", "<span>", "</span>");
                            }

                            string ReviewDate = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"review-date\"", ">", "<").Trim();
                            if (ReviewDate.Contains("on"))
                            {
                                ReviewDate = commonHelper.After(ReviewDate, "on");
                                cutomerreview.ReviewDate = Convert.ToDateTime(ReviewDate.Trim());
                            }
                            else
                            {
                                cutomerreview.ReviewDate = Convert.ToDateTime(HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"review-date\"", ">", "<").Trim());
                            }
                            cutomerreview.ReviewRating = rating;//Convert.ToDecimal(HtmlHelper.ReturnValue(stringArray[j], "class=\"a-link-normal\"", "title=\"", " out of 5 stars"));

                            cutomerreview.Description = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"review-body\"", "<span class=\"cr-original-review-content\">", "</span>");
                            if (cutomerreview.Description == "" || cutomerreview.Description == null)
                            {
                                cutomerreview.Description = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"review-body\"", "<span>", "</span>");
                            }
                            //cutomerreview.Color = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"format-strip\"", "Color: ", "</a>");
                            cutomerreview.Color = Regex.Replace(HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"format-strip\"", "Color: ", "<"), "<.*?>", string.Empty);
                            cutomerreview.Size = Regex.Replace(HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"format-strip\"", "Size: ", "<"), "<.*?>", string.Empty);
                            cutomerreview.PeopleHelpfulCount = Convert.ToInt32(HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"helpful-vote-statement\"", ">", "people found this helpful").Trim() == "" ? null : HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"helpful-vote-statement\"", ">", "people found this helpful").Replace(",", "").Trim());
                            cutomerreview.IsVerifiedPurchase = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"avp-badge\"", ">", "<") != "" ? true : false;
                            cutomerreview.IsEarlyReviewerRewards = stringArray[j].Trim().Contains(">Early Reviewer Rewards<".Trim()) ? true : false;
                            cutomerreview.IsPositiveReview = rating > 3 ? true : false;
                            string CustomerLink = HtmlHelper.ReturnValue(stringArray[j], "data-hook=\"genome-widget\"", "href=\"", "\"");

                            string[] splitUrl = CustomerLink.Split('/');
                            //cutomerreview.ReviewedBy = HtmlHelper.ReturnValue(stringArray[j], "href=\"/gp/profile", "/", "/ref=");
                            cutomerreview.ReviewedBy = splitUrl[3];

                            lstCutomerReview.Add(cutomerreview);

                            if (lstCutomer.Any(x => x.AmazonUserId == cutomerreview.ReviewedBy))
                            {

                            }
                            else
                            {
                                Customer customer = new Customer();
                                customer = getCustomerDetail(domainUrl, CustomerLink, ASIN, customerName);
                                lstCutomer.Add(customer);
                            }
                        }
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    ExceptionHandling.CreateException(ex, "Product Review: " + ASIN.ToString(), null);
                }
            }
            customerLst = lstCutomer;
            return lstCutomerReview;
        }
        public Customer getCustomerDetail(string domainUrl, string url, string ASIN, string customerName)
        {
            string[] splitUrl = url.Split('/');

            string boiUrl = string.Empty;
            string dashBoardUrl = string.Empty;
            string profileImageUrl = string.Empty;
            string imazonUserId = string.Empty;
            if (splitUrl[3].Contains("amzn1.account"))
            {
                boiUrl = "profilewidget/bio/" + splitUrl[3] + "?view=visitor";
                dashBoardUrl = "hz/gamification/api/contributor/dashboard/" + splitUrl[3] + "?ownerView=false&customerFollowEnabled=false";
                profileImageUrl = "avatar/default.json?customer_id=" + splitUrl[3];
                imazonUserId = splitUrl[3];
            }
            else
            {
                return null;
            }



            int bioSourceretry = 0;
        bioSource_retry:
            var bioSource = commonHelper.GetSource(domainUrl, boiUrl, "");
            #region RETRY if Source is Empty
            if (bioSource == "" && bioSourceretry < Convert.ToInt32(ConfigurationSettings.AppSettings["_retry"]))
            {
                bioSourceretry++;

                goto bioSource_retry;
            }
            else if (bioSource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
            {
                bioSourceretry++;

                goto bioSource_retry;
            }
            #endregion

            int dashBoarSourceretry = 0;
        dashBoarSource_retry:
            var dashBoarSource = commonHelper.GetSource(domainUrl, dashBoardUrl, "");
            #region RETRY if Source is Empty
            if (dashBoarSource == "" && dashBoarSourceretry < Convert.ToInt32(ConfigurationSettings.AppSettings["_retry"]))
            {
                dashBoarSourceretry++;
                goto dashBoarSource_retry;
            }
            else if (dashBoarSource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
            {
                dashBoarSourceretry++;
                goto dashBoarSource_retry;
            }
            #endregion

            int profileImageSourceretry = 0;
        profileImageSource_retry:
            var profileImageSource = commonHelper.GetSource(domainUrl, profileImageUrl, "");
            #region RETRY if Source is Empty
            if (profileImageSource == "" && profileImageSourceretry < Convert.ToInt32(ConfigurationSettings.AppSettings["_retry"]))
            {
                profileImageSourceretry++;
                goto profileImageSource_retry;
            }
            else if (profileImageSource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
            {
                profileImageSourceretry++;
                goto profileImageSource_retry;
            }
            #endregion


            Customer cust = new Customer();
            cust.AmazonUserId = imazonUserId;
            cust.CustomerFrontURL = domainUrl + url;
            cust.ProfileImageURL = HtmlHelper.ReturnValue(profileImageSource, "\"url\":", "\"", "\",");
            cust.Name = customerName;
            cust.Designation = HtmlHelper.ReturnValue(bioSource, "\"occupation\"", ":", ",").Replace("null", "");
            cust.About = HtmlHelper.ReturnValue(bioSource, "\"personalDescription\"", ":", ",\"showAboutMe\"");

            string ReviewRanking = HtmlHelper.ReturnValue(bioSource, "\"topReviewerInfo\":", "\"rank\":", ",\"decoratedRank").Replace("null", "");
            cust.ReviewRanking = long.Parse(ReviewRanking == "" ? "0" : ReviewRanking);


            string HelpfulVotes = HtmlHelper.ReturnValue(dashBoarSource, "\"helpfulVotesData\":", "\"count\":\"", "\",").Replace(",", "").Replace("-", "");
            cust.HelpfulVotes = Convert.ToInt32(HelpfulVotes == "" ? "0" : HelpfulVotes);
            string Review = HtmlHelper.ReturnValue(dashBoarSource, "\"reviewsCountData\":", "\"count\":\"", "\",").Replace(",", "").Replace("-", "");
            cust.Reviews = Convert.ToInt32(Review == "" ? "0" : Review);
            string Hearts = HtmlHelper.ReturnValue(dashBoarSource, "\"ideaListHeartsData\":", "\"count\":\"", "\",").Replace(",", "").Replace("-", "");
            cust.Hearts = Convert.ToInt32(Hearts == "" ? "0" : Hearts);
            string IdeaLists = HtmlHelper.ReturnValue(dashBoarSource, "\"ideaListData\":", "\"count\":\"", "\",").Replace(",", "").Replace("-", "");
            cust.IdeaLists = Convert.ToInt32(IdeaLists == "" ? "0" : IdeaLists);

            return cust;
        }

        #endregion
    }
}
