﻿using AmazoneASINScrapper.DAL.DBEntity;
using AmazoneASINScrapper.helper;
using ScrapingHelp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazoneASINScrapper.BAL.Scrapper
{
    public class sellerInfo
    {
        #region PRODUCT SELLER DATA

        public List<ProductSellerMapping> getproductSellerMapping(string domainUrl, string ASIN, out List<Seller> sellerLst,string AmazonSellerID)
        {

            List<Seller> lstSeller = new List<Seller>();
            List<ProductSellerMapping> lstproductsellermapping = new List<ProductSellerMapping>();

            int sellerPageCout = 0;
            int pagesize = 1;
            int pageCount = 2;
            while (pageCount != pagesize)
            {
                try
                {
                    int retry = 0;
                    sellerSource_retry:
                    var sellerSource = commonHelper.GetSource(domainUrl, "gp/offer-listing/" + ASIN + "/ref=olp_page_" + pagesize + "?ie=UTF8&f_all=true&startIndex=" + sellerPageCout + "", "");

                    #region RETRY if Source is Empty
                    if (sellerSource == "" && retry < Convert.ToInt32(ConfigurationSettings.AppSettings["_retry"]))
                    {
                        retry++;

                        goto sellerSource_retry;
                    }
                    else if (sellerSource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
                    {
                        retry++;

                        goto sellerSource_retry;
                    }
                    #endregion

                    string[] stringArray = sellerSource.Split(new string[] { "class=\"a-row a-spacing-mini olpOffer\"" }, StringSplitOptions.None);


                    for (int j = 1; j < stringArray.Length; j++)
                    {
                        Seller seller = new Seller();
                        string isAmazonFulfilled = HtmlHelper.ReturnValue(stringArray[j], "class=\"a-size-medium a-text-bold\"", "isAmazonFulfilled=", "&");
                        string sellerID = HtmlHelper.ReturnValue(stringArray[j], "class=\"a-size-medium a-text-bold\"", ";seller=", "\"");
                        string sellerUrlParram = "sp?_encoding=UTF8&isAmazonFulfilled=" + isAmazonFulfilled + "&seller=" + sellerID;

                        ProductSellerMapping productsellermapping = new ProductSellerMapping();
                        productsellermapping.ASIN = ASIN;
                        productsellermapping.Condition = HtmlHelper.ReturnValue(stringArray[j], "class=\"a-size-medium olpCondition a-text-bold\"", ">", "</span>");
                        productsellermapping.Price = HtmlHelper.ReturnValue(stringArray[j], "class=\"currencyINRFallback\"", "</span>", "</span>");

                        productsellermapping.IsPrime = HtmlHelper.ReturnValue(stringArray[j], "class=\"supersaver\"", "</span>").Contains("a-icon a-icon-prime") ? false : true;
                        productsellermapping.IsFulfillmentbyAmazon = isAmazonFulfilled.Trim() == "1" ? true : false;

                        productsellermapping.AmazonSellerId = sellerID;


                        lstproductsellermapping.Add(productsellermapping);
                        if (productsellermapping.AmazonSellerId != "")
                        {
                            seller = getSellerDetail(domainUrl, sellerUrlParram,sellerID);
                            seller.AmazonSellerId = sellerID;
                            lstSeller.Add(seller);
                        }
                        else
                        {
                            productsellermapping.AmazonSellerId = HtmlHelper.ReturnValue(stringArray[j], "<img", "alt=\"", "\"");
                        }
                        //seller.ProductSellerMappings = lstproductsellermapping;

                    }

                    if (sellerPageCout == 0)
                    {
                        string short_src = HtmlHelper.ReturnValue(sellerSource, "class=\"a-pagination\"", "</ul>");
                        string[] pagingArray = short_src.Split(new string[] { "</li>" }, StringSplitOptions.None);
                        pageCount = pagingArray.Length - 3;
                        if (pageCount < 1)
                            break;
                    }
                    pagesize++;
                    sellerPageCout = sellerPageCout + 10;

                }
                catch (Exception ex)
                {
                    ExceptionHandling.CreateException(ex, "Seller Mapping Info: " + ASIN.ToString(), null);
                }
            }

            sellerLst = lstSeller;
            return lstproductsellermapping;
            // return lstSeller;
        }
        public Seller getSellerDetail(string domainUrl, string sellerprofileUrl, string AmazonSellerId)
        {
            Seller seller = new Seller();

            int retry = 0;
            sellerProfileSource_retry:
            var sellerSource = commonHelper.GetSource(domainUrl, sellerprofileUrl, "");
            #region RETRY if Source is Empty
            if (sellerSource == "" && retry < Convert.ToInt32(ConfigurationSettings.AppSettings["_retry"]))
            {
                retry++;

                goto sellerProfileSource_retry;
            }
            else if (sellerSource.Replace(" ", "").Contains("<title dir=\"ltr\">Robot Check</title>".Replace(" ", "")))
            {
                retry++;

                goto sellerProfileSource_retry;
            }
            #endregion

            seller.SellerName = HtmlHelper.ReturnValue(sellerSource, "id=\"sellerName\"", ">", "<");
            seller.SellerFrontURL = sellerprofileUrl;
            seller.SellerShopURL = domainUrl + HtmlHelper.ReturnValue(sellerSource, "id=\"products-link\"", "href=\"", "\">Products</a>");
            seller.ProfileImageURL = HtmlHelper.ReturnValue(sellerSource, "id=\"seller-logo-container\"", "src=\"", "\"");
            seller.Review= HtmlHelper.ReturnValue(sellerSource, "class=\"a-link-normal feedback-detail-description\"", "(", " ratings)");
            if(seller.Review.Contains("total"))
            {
                seller.Review = seller.Review.Replace("total", "").TrimEnd(' ');
            }
            
            seller.Rating = HtmlHelper.ReturnValue(sellerSource, "<i class=\"a-icon a-icon-star a-star-4-5 feedback-detail-stars\">", "<span class=\"a-icon-alt\">", " out of 5 stars");
            if(seller.Rating==string.Empty)
            {
                seller.Rating = HtmlHelper.ReturnValue(sellerSource, "<i class=\"a-icon a-icon-star a-star-4 4.\">", "<span class=\"a-icon-alt\">", " out of 5 stars");
            }
            if(seller.Rating==string.Empty)
            {
                //<i class="a-icon a-icon-star a-star-5 feedback-detail-stars"><span class="a-icon-alt">5 out of 5 stars</span></i>
                seller.Rating = HtmlHelper.ReturnValue(sellerSource, "<span class=\"a-icon-alt\">", "out of 5 stars");
            }
            seller.WhyChooseUs = HtmlHelper.ReturnValue(sellerSource, "id=\"about-seller-expanded\"", ">", "</span>");
            seller.CustomerServicePhone = "";

            string short_src = HtmlHelper.ReturnValue(sellerSource, "id=\"feedback-summary-table\"", "</table>");
            string[] stringArray = HtmlHelper.CollectUrl(short_src, "", "<tr>", "</tr>");
            for (int i = 1; i < stringArray.Length; i++)
            {
                string[] feedbackArray = stringArray[i].Split(new string[] { "</td>" }, StringSplitOptions.None);
                if (feedbackArray.Length > 4)
                {
                    if (feedbackArray[0].Contains("Positive"))
                    {
                        string days30 = HtmlHelper.ReturnValue(feedbackArray[1].Replace("-", "").Replace(" ", ""), " < span", ">", "</span>").Replace(",", "");
                        string days90 = HtmlHelper.ReturnValue(feedbackArray[2].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string month12 = HtmlHelper.ReturnValue(feedbackArray[3].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string LifeTime = HtmlHelper.ReturnValue(feedbackArray[4].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        seller.Positive_30days = Convert.ToInt32(days30 != "" ? days30 : null);
                        seller.Positive_90days = Convert.ToInt32(days90 != "" ? days90 : null);
                        seller.Positive_12Month = Convert.ToInt32(month12 != "" ? month12 : null);
                        seller.Positive_LifeTime = Convert.ToInt32(LifeTime != "" ? LifeTime : null);

                    }

                    if (feedbackArray[0].Contains("Neutral"))
                    {
                        string days30 = HtmlHelper.ReturnValue(feedbackArray[1].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string days90 = HtmlHelper.ReturnValue(feedbackArray[2].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string month12 = HtmlHelper.ReturnValue(feedbackArray[3].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string LifeTime = HtmlHelper.ReturnValue(feedbackArray[4].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        seller.Neutral_30days = Convert.ToInt32(days30 != "" ? days30 : null);
                        seller.Neutral_90days = Convert.ToInt32(days90 != "" ? days90 : null);
                        seller.Neutral_12Month = Convert.ToInt32(month12 != "" ? month12 : null);
                        seller.Neutral_LifeTime = Convert.ToInt32(LifeTime != "" ? LifeTime : null);

                    }
                    if (feedbackArray[0].Contains("Negative"))
                    {
                        string days30 = HtmlHelper.ReturnValue(feedbackArray[1].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string days90 = HtmlHelper.ReturnValue(feedbackArray[2].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string month12 = HtmlHelper.ReturnValue(feedbackArray[3].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        string LifeTime = HtmlHelper.ReturnValue(feedbackArray[4].Replace("-", "").Replace(" ", ""), "<span", ">", "</span>").Replace(",", "");
                        seller.Negative_30days = Convert.ToInt32(days30 != "" ? days30 : null);
                        seller.Negative_90days = Convert.ToInt32(days90 != "" ? days90 : null);
                        seller.Negative_12Month = Convert.ToInt32(month12 != "" ? month12 : null);
                        seller.Negative_LifeTime = Convert.ToInt32(LifeTime != "" ? LifeTime : null);

                    }
                }

            }
            //string []arr = sellerprofileUrl.Split('=');
            //string AmazonSellerId = arr[1];
            string MarketPlaceID = string.Empty;
            if (AmazonSellerId != "")
            {
                string param = "/s?me=" + AmazonSellerId;
                string contentSellerProd = commonHelper.GetSource(domainUrl, param, "");
                string sellerProduct = HtmlHelper.ReturnValue(contentSellerProd, "<span dir=\"auto\">", " results for</span>");
                if(sellerProduct.Contains("of"))
                {
                    // < span dir = "auto" > 1 - 16 of 20 results for</ span >
                    if (sellerProduct.Contains("over"))
                    {
                        string res = HtmlHelper.ReturnValue(contentSellerProd, "<span dir=\"auto\">", "of ", " results for").Replace("over", "");
                        if (res.Contains(","))
                        {
                            res = res.Replace(",", "");
                        }
                        seller.SellerProductCount = res != "" ? Convert.ToInt32(res) : 0;
                    }
                    else
                    {
                        string a = HtmlHelper.ReturnValue(contentSellerProd, "<span dir=\"auto\">", "</span>");
                        seller.SellerProductCount = HtmlHelper.ReturnValue(a, "of", "results for") != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(a, "of", "results for")) : 0;
                    }
                }
                //seller.SellerProductCount = HtmlHelper.ReturnValue(contentSellerProd, "<span dir=\"auto\">", " results for</span>") != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(contentSellerProd, "<span dir=\"auto\">", " results for</span>")) : 0;
                if(seller.SellerProductCount==0)
                {
                    if (HtmlHelper.ReturnValue(contentSellerProd, "<span>", "of ", " results for").Contains("over"))
                    {
                        string res = HtmlHelper.ReturnValue(contentSellerProd, "<span>", "of ", " results for").Replace("over", "");
                        if (res.Contains(","))
                        {
                            res = res.Replace(",", "");
                        }
                        seller.SellerProductCount = res != "" ? Convert.ToInt32(res) : 0;
                    }
                    else { 
                    seller.SellerProductCount = HtmlHelper.ReturnValue(contentSellerProd, "<span>","of ", " results for") != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(contentSellerProd, "<span>", "of ", " results for")) : 0;
                    }
                }
                if (seller.SellerProductCount > 0)
                {
                    string[] MID = new string[] { };
                    MID = HtmlHelper.ReturnValue(contentSellerProd, "src='//fls-na.amazon.com/1/batch/1/OP/", " :").Split(':');
                    MarketPlaceID = MID[0];
                    if(MarketPlaceID=="")
                    {
                       string Mid = HtmlHelper.ReturnValue(contentSellerProd, "<img src=\"https://assoc-na.associates-amazon.com/abid/um?s=", " style");
                        MID = Mid.Split('=');
                        if (MID[0] != "")
                        {
                            MarketPlaceID = MID[1] != "" ? MID[1].Replace("\"", "") : "";
                        }
                    }

                    if(MarketPlaceID=="")
                    {
                        string mid = HtmlHelper.ReturnValue(contentSellerProd, "'ObfuscatedMarketplaceId':", ",");
                        MarketPlaceID = mid.Replace("'", "");
                    }

                    seller.MarketPlaceID = MarketPlaceID;
                }
            }
            return seller;
        }
        #endregion
    }
}
