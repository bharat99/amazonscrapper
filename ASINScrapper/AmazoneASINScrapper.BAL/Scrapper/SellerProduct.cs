﻿using AmazoneASINScrapper.BAL.helper;
using AmazoneASINScrapper.DAL.DBEntity;
using HtmlAgilityPack;
using ScrapingHelp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazoneASINScrapper.BAL.Scrapper
{
    public class SellerProduct
    {
        public void saveSellerProducts(string domainUrl, string sellerID, string MarketPlaceID,int sellerProducts)
        {
            ProductDetail p = new ProductDetail();
            productInfo pInfo = new Scrapper.productInfo();
            productReviewInfo clsreview = new productReviewInfo();
            QnAInfo clsqna = new QnAInfo();
            List<Customer> customer = new List<Customer>();
            string spASIN = string.Empty;
            string param = string.Empty;
            int totalPage = 0;
            int PerPageProduct = 16;
            if (sellerProducts > 0)
            {
                totalPage = Convert.ToInt16(Math.Round(Convert.ToDecimal((sellerProducts / PerPageProduct))));
                for (int i = 1; i <= totalPage; i++)
                {
                    //https://www.amazon.in/s?i=merchant-items&me=A266UCF4VI0ULL&marketplaceID=A21TJRUUN4KGV&qid=1589202022&ref=sr_pg_1
                    if (i == 1)
                    {
                        param = string.Format("s?me={0}&marketplaceID={1}&ref=sr_pg_{2}", sellerID, MarketPlaceID, i);
                    }
                    else
                    {
                        //https://www.amazon.in/s?i=merchant-items&me=A266UCF4VI0ULL&page=2&marketplaceID=A21TJRUUN4KGV&qid=1589204007&ref=sr_pg_2
                        param = string.Format("s?me={0}&page={2}&marketplaceID={1}&&ref=sr_pg_{2}", sellerID, MarketPlaceID, i);
                    }

                    int retry = 0;
                retry_mainsource:
                    var mainsource = commonHelper.GetSource(domainUrl, param, "");
                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(mainsource);
                    #region RETRY if Source is Empty
                    if (mainsource == "" && retry < Convert.ToInt32(ConfigurationSettings.AppSettings["_retry"]))
                    {
                        retry++;

                        goto retry_mainsource;
                    }
                    #endregion
                    string[] asinsProducts = new string[] { };
                    string[] asin = new string[] { };
                    asinsProducts = mainsource.Split(new string[] { "<div class=\"s-main-slot s-result-list s-search-results sg-row\">" }, StringSplitOptions.None);
                    asin = asinsProducts[1].Split(new string[] { "<div data-" }, StringSplitOptions.None);

                    if(!asinsProducts[1].Contains("div data-asin") || !asinsProducts[1].Contains("asin"))
                    {
                        asinsProducts = mainsource.Split(new string[] { "<div class=\"s-result-list s-search-results sg-row\">" }, StringSplitOptions.None);
                        asin = asinsProducts[1].Split(new string[] { "<div data-" }, StringSplitOptions.None);
                    }
                    

                    try
                    {
                        if (asin.Length > 0)
                        {

                            for (int ii = 1; ii <= asin.Length; ii++)
                            {

                                spASIN = HtmlHelper.ReturnValue(asin[ii], "asin=\"", "\"") != "" ? HtmlHelper.ReturnValue(asin[ii], "asin=\"", "\"") : "";

                                if (!string.IsNullOrEmpty(spASIN))
                                {
                                    timeLogger.updateCreateScheduleTable(spASIN);

                                    p = pInfo.getProductDetail(domainUrl, spASIN);
                                    //p.CustomerReviews = clsreview.getCutomerReview(domainUrl, spASIN, out customer);
                                    p.ProductQuestions = clsqna.getFAQ(domainUrl, spASIN);
                                    p.MarketPlaceID = MarketPlaceID;
                                    var qid = doc.GetElementbyId("qid");
                                    if (qid != null)
                                        p.Qid = qid.InnerHtml;//<input type="hidden" name="qid" value="1589204070"/>
                                    BusinessRepo.ProductRepo repo = new BusinessRepo.ProductRepo();

                                    repo.saveScrapedDetail(p, customer, new List<Seller>());
                                }

                            }
                        }
                    }

                    catch (Exception ex)
                    {

                        throw;
                    }
                }
            }
        }

        public void SaveSellerProduct(ProductDetail p, string ASIN, string SellerID)
        {

        }
    }
}
