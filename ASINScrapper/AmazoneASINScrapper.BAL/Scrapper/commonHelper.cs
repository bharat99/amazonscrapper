﻿using AmazoneASINScrapper.helper;
using ScrapingHelp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AmazoneASINScrapper.BAL.Scrapper
{
    public static class commonHelper
    {

        public static string Between(string value, string a, string b)
        {
            int posA = value.IndexOf(a);
            int posB = value.LastIndexOf(b);
            if (posA == -1)
            {
                return "";
            }
            if (posB == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= posB)
            {
                return "";
            }
            return value.Substring(adjustedPosA, posB - adjustedPosA);
        }

        /// <summary>
        /// Get string value after [first] a.
        /// </summary>
        public static string Before(string value, string a)
        {
            int posA = value.IndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            return value.Substring(0, posA);
        }

        /// <summary>
        /// Get string value after [last] a.
        /// </summary>
        public static string After(string value, string a)
        {
            int posA = value.LastIndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= value.Length)
            {
                return "";
            }
            return value.Substring(adjustedPosA);
        }



        #region GETSOURCE AND PROXY SETTINGS
        public static string GetSource(string _url, string _param, string scookie)
        {

            int retry = 0;
        tryagain:

            int ProxyChange = 0;
            try
            {

                System.Net.ServicePointManager.Expect100Continue = false;
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AllwaysGoodCertificate);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                HttpWebRequest myWebRequest = (HttpWebRequest)HttpWebRequest.Create(_url + _param);
                myWebRequest.KeepAlive = true;
                myWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                myWebRequest.Method = WebRequestMethods.Http.Get;
                myWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36";
                myWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                myWebRequest.Headers["Accept-Language"] = "en-US,en;q=0.9";
                myWebRequest.Headers["accept-encoding"] = "gzip, deflate, br";
                myWebRequest.Host = "www.amazon.com";
                // myWebRequest.Headers["Cookie"] = "PHPSESSID=fp9ed5gobi6nk1bv0jq59fs4di; StickySession=id.65889726442.546www.investing.com";
                myWebRequest.Headers["X-Requested-With"] = "XMLHttpRequest";

                if (Convert.ToBoolean(ConfigurationSettings.AppSettings["_proxy"].Trim()))
                {
                    myWebRequest.Proxy = SetProxySevere(++ProxyChange);
                    if (ProxyChange >= 16)
                        ProxyChange = 0;
                }
                myWebRequest.Timeout = Timeout.Infinite;
                myWebRequest.AllowAutoRedirect = false;

                HttpWebResponse myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                Stream responseStream = myWebResponse.GetResponseStream();
                StreamReader myWebSource = new StreamReader(responseStream);
                string myPageSource = myWebSource.ReadToEnd();
                myWebResponse.Close();
                responseStream.Close();

                return HtmlHelper.ReturnFormatedHtml(myPageSource);
            }
            catch (Exception ex)
            {
                if (retry >= 5)
                {
                    ExceptionHandling.CreateException(ex, "WebRequest Error : " + "GetSource: " + _url + _param, null);
                }
                else
                {
                    retry++;
                    Thread.Sleep(10000);
                    goto tryagain;
                }
                return string.Empty;
            }

        }

        public static WebProxy SetProxySevere(int ChangeCount)
        {
            WebProxy setproxy = new WebProxy("world.proxymesh.com", 31280);
            if (ChangeCount <= 4)
            {
                setproxy = new WebProxy("us.proxymesh.com", 31280);
            }
            else
            {
                if (ChangeCount <= 8)
                {
                    setproxy = new WebProxy("us-il.proxymesh.com", 31280);
                }
                else
                {
                    if (ChangeCount <= 12)
                    {
                        setproxy = new WebProxy("us-fl.proxymesh.com", 31280);
                    }
                    else
                    {
                        if (ChangeCount <= 16)
                        {
                            setproxy = new WebProxy("us-ca.proxymesh.com", 31280);
                        }
                        else
                        {
                            setproxy = new WebProxy("us-dc.proxymesh.com", 31280);
                            ChangeCount = 0;
                        }
                    }
                }

            }
            return setproxy;
        }

        private static bool AllwaysGoodCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
        {
            return true;

        }
        #endregion

        public static string Postsourcekrogergroup(string url, string Cookie, string PostData, string HttpReferer, string ContentType)
        {
            try
            {
                string _Website = string.Empty;
                string _Token = string.Empty;
                string _DivisionId = string.Empty;
                string _StoreId = string.Empty;

                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AllwaysGoodCertificate);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpWebRequest myWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                myWebRequest.Method = WebRequestMethods.Http.Post;
                myWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                myWebRequest.KeepAlive = true;
                myWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko";
                myWebRequest.Headers["Accept-Language"] = "en-US";
                myWebRequest.Headers["Accept-Encoding"] = "gzip, deflate";
                myWebRequest.Accept = "application/json, text/plain, */*";
                myWebRequest.Host = _Website.Replace("https://", "");
                myWebRequest.Referer = HttpReferer.Replace("é", "%C3%A9");

                myWebRequest.Headers["origin"] = _Website;
                myWebRequest.Headers["X-XSRF-TOKEN"] = _Token;
                myWebRequest.Headers["SEC_REQ_TYPE"] = "ajax";
                myWebRequest.Headers["X-Division-Id"] = _DivisionId;
                myWebRequest.Headers["X-Store-Id"] = _StoreId;
                myWebRequest.Timeout = Timeout.Infinite;
                myWebRequest.Headers["Cookie"] = Cookie;

                myWebRequest.ContentLength = PostData.Length;
                myWebRequest.ContentType = ContentType;

                StreamWriter writer = new StreamWriter(myWebRequest.GetRequestStream());
                writer.Write(PostData);
                writer.Close();

                HttpWebResponse myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                Stream responseStream = myWebResponse.GetResponseStream();
                StreamReader myWebSource = new StreamReader(responseStream, Encoding.UTF8);
                string myPageSource = myWebSource.ReadToEnd();
                myWebResponse.Close();
                responseStream.Close();
                return myPageSource;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }



    }
}
