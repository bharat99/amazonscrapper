﻿namespace AmazoneASINScrapper.helper
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Json;
    using System.Runtime.Serialization.Json;
    using System.Text;
    using System.Threading;

    public static class ExceptionHandling
    {
        public static void CreateException(
            Exception exception,
            string methodName,
            Dictionary<string, object> argumentsDictionary)

        {
            var type = exception.GetType();
            var message = exception.Message;
            var stackTrace = exception.StackTrace;
            var apiName = methodName;
            var arg = string.Empty;

            if (argumentsDictionary != null)
            {
                var arguments = argumentsDictionary.FirstOrDefault().Value;
                var ms = new MemoryStream();
                if (arguments != null)
                {
                    var js = new DataContractJsonSerializer(arguments.GetType());
                    js.WriteObject(ms, arguments);
                }

                ms.Position = 0;
                var sr = new StreamReader(ms);
                arg = sr.ReadToEnd();
                sr.Close();
                ms.Close();
            }

            //if (Convert.ToBoolean(ConfigurationManager.AppSettings["DBLogging"]))
            //{

            //}
            //else
            //{
            FileLogging(type.ToString(), message, stackTrace, apiName, string.Empty, 0, 0, arg);
            //}
        }
       static EventWaitHandle waitHandle = new EventWaitHandle(true, EventResetMode.AutoReset, "SHARED_BY_ALL_PROCESSES");
        public static void FileLogging(
            string exceptionType,
            string message,
            string stackTrace,
            string apiName,
            string sessionKey,
            int userId,
            int roleId,
            string arguments)
        {
            var folderName = DateTime.Now.Date.ToString("MM-dd-yyyy");
            var exceptionLoggingPath = string.Concat(AppDomain.CurrentDomain.BaseDirectory, Convert.ToString(ConfigurationSettings.AppSettings["ExceptionLoggingUrl"]), folderName);
            if (!Directory.Exists(exceptionLoggingPath))
            {
                Directory.CreateDirectory(exceptionLoggingPath);
            }
            var fileName = DateTime.Now.ToString("hh tt");
            exceptionLoggingPath = string.Concat(exceptionLoggingPath, "/", fileName, ".txt");
            var exception = new StringBuilder();
            exception.AppendLine(string.Concat("ErrorMessage:", message));
            exception.AppendLine(string.Concat("ExceptionType:", exceptionType));
            exception.AppendLine(string.Concat("CreatedDate:", DateTime.Now.ToString("yyyy MMMM dd hh:mm:ss tt")));
            exception.AppendLine(string.Concat("APIName:", apiName));
            exception.AppendLine(string.Concat("SessionKey:", sessionKey));
            exception.AppendLine(string.Concat("Arguments:", arguments));
            exception.AppendLine(string.Concat("UserId:", userId));
            exception.AppendLine(string.Concat("UserRole:", roleId));
            exception.AppendLine(string.Concat("StackTrace:", string.Empty));
            exception.AppendLine(stackTrace);
            exception.AppendLine();
            exception.AppendLine();
            exception.AppendLine();
            exception.AppendLine();
            exception.AppendLine();
            exception.AppendLine("--------------------------------------------------------------------------------------------------------");
            waitHandle.WaitOne();
            File.AppendAllText(exceptionLoggingPath, exception.ToString());
            waitHandle.Set();
        }
    }
}