﻿using AmazoneASINScrapper.DAL.DBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazoneASINScrapper.BAL.helper
{
    public static class timeLogger
    {

        public static void updateCreateScheduleTable(string ASIN)
        {
            using (AmazonDB context = new AmazonDB())
            {
                var record = context.ASINs.Where(x => x.ASIN1 == ASIN).FirstOrDefault();
                if (record != null)
                {
                    record.IsScheduled = true;
                    record.Status = "Running";
                    record.StartedOn = DateTime.Now;
                    context.SaveChanges();
                }
                else
                {
                    ASIN _asin = new ASIN();
                    _asin.ASIN1 = ASIN;
                    _asin.IsScheduled = true;
                    _asin.ScheduleOn = DateTime.Now;
                    _asin.Status = "Running";
                    _asin.StartedOn = DateTime.Now;
                    context.ASINs.Add(_asin);
                    context.SaveChanges();
                }
            }
        }
    }
}

